import React from 'react';
import { StyleSheet, } from "react-native";
import TextView from './TextView';
import PropTypes from "prop-types";
import { Button, } from 'native-base';
import AppConstants from '../utils/AppConstants';

class CreateButton extends React.Component {
    render() {
        return (
            <Button
                rounded
                light
                style={[styles.CreateButton, this.props.buttonStyle]}
                onPress={this.props.onPress}>
                <TextView
                    style={[styles.CreateButtonText, this.props.textStyle]}
                    numberOfLines={1}>{this.props.title}</TextView>
            </Button>
        );
    }
}

CreateButton.props = {
    onPress: PropTypes.func,
    title: PropTypes.string,
    buttonStyle: PropTypes.object,
    textStyle: PropTypes.object,
};

CreateButton.defaultProps = {
    onPress: () => { },
    title: "",
    buttonStyle: {},
    textStyle: {},
};

const styles = StyleSheet.create({

    CreateButton: {
        backgroundColor: AppConstants.COLOR_WHITE,
        alignSelf: 'center',
        height: 40,
    },
    CreateButtonText: {
        textAlign: 'center',
        color: AppConstants.COLOR_SKY_BLUE,
        paddingHorizontal: 16,
    },
});

export default CreateButton;