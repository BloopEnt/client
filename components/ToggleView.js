import {Animated, PanResponder, Text, TouchableOpacity, View,} from 'react-native';
import React, {Component} from 'react';
import PropTypes from 'prop-types';

const Button = props => {
    return (
        <View>
            <TouchableOpacity
                onPress={props.onPress}
                style={{
                    flex: 1,
                    width: props.width,
                    height: props.height,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                <Text>{props.text}</Text>
            </TouchableOpacity>
        </View>
    );
};

Button.propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    text: PropTypes.string,
    onPress: PropTypes.func
};

export default class ToggleView extends Component {

    constructor(props) {
        super(props);

        const width = this.props.width;

        this.state = {
            isComponentReady: false,
            position: new Animated.Value(0),
            posValue: 0,
            selectedPosition: 0,
            enabled: this.props.enabled,
            duration: 100,
            width: width,
            switcherWidth: width / 2,
            thresholdDistance: width - width / 2,
        };
        this.isParentScrollDisabled = false;
    }

    componentDidMount() {

        if (this.state.enabled) {
            // console.log('this.state.enabled: ' + this.state.enabled);
            this.completeSelected();
        } else {
            // console.log('this.state.enabled: ' + this.state.enabled);
            this.notStartedSelected();
        }
    }

    notStartedSelected = () => {
        if (this.state.selectedPosition === 0) {
            return;
        }

        Animated.timing(this.state.position, {
            toValue: 0,
            duration: this.state.duration
        }).start();

        setTimeout(() => {
            this.setState({
                posValue: 0,
                selectedPosition: 0
            });
            this.props.onStatusChanged(false);
        }, 0);
    };

    completeSelected = () => {
        if (this.state.selectedPosition === 1) {
            return;
        }

        Animated.timing(this.state.position, {
            toValue: this.state.switcherWidth,
            duration: this.state.duration
        }).start();

        setTimeout(() => {
            this.setState({
                posValue: this.state.switcherWidth,
                selectedPosition: 1
            });
            this.props.onStatusChanged(true);
        }, 0);
    };

    getStatus = () => {
        switch (this.state.selectedPosition) {
            case 0:
                return this.props.inactiveText;
            case 1:
                return this.props.activeText;
        }
    };

    render() {
        const width = this.props.width;
        const buttonWidth = this.state.switcherWidth;
        const height = this.props.height;

        return (
            <View style={{
                width: width,
                height: height,
                flexDirection: 'row',
                backgroundColor: this.props.backgroundColor,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: height / 2,
            }}>
                <Button
                    width={buttonWidth}
                    height={height}
                    text={this.props.inactiveText}
                    onPress={this.notStartedSelected}/>
                <Button
                    width={buttonWidth}
                    height={height}
                    text={this.props.activeText}
                    onPress={this.completeSelected}/>
                <Animated.View
                    style={{
                        flexDirection: 'row',
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        backgroundColor: this.props.buttonColor,
                        alignItems: 'center',
                        justifyContent: 'center',
                        elevation: 4,
                        shadowOpacity: 0.31,
                        shadowRadius: 10,
                        shadowColor: "#ffffff",
                        width: buttonWidth - 4,
                        height: height - 4,
                        borderRadius: height / 2,
                        marginHorizontal: 2,
                        marginVertical: 2,
                        transform: [{translateX: this.state.position}],
                    }}>
                    <Button
                        width={buttonWidth}
                        height={height}
                        text={this.getStatus()}/>
                </Animated.View>
            </View>
        );
    }
}

ToggleView.propTypes = {

    enabled: PropTypes.bool,

    width: PropTypes.number,
    height: PropTypes.number,

    activeText: PropTypes.string,
    inactiveText: PropTypes.string,

    onStatusChanged: PropTypes.func,

    backgroundColor: PropTypes.string,
    buttonColor: PropTypes.string,
};

ToggleView.defaultProps = {

    enabled: false,

    width: 100,
    height: 40,

    activeText: 'On',
    inactiveText: 'Off',

    backgroundColor: "#707070",
    buttonColor: '#FFFFFF',

    onStatusChanged: (enabled) => {
        console.log(`value changed: ${enabled}`)
    },

};
