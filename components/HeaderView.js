import React from 'react';
import { Image, TouchableWithoutFeedback, View, StyleSheet, } from 'react-native';
import PropTypes from "prop-types";


export default class HeaderView extends React.Component {

    static propTypes = {
        icon: PropTypes.number,
        onPressIcon: PropTypes.func,
    };

    render() {
        return (
            <View style={styles.container}>

                <Image
                    style={styles.leftIcon}
                    source={require('../assets/icons/icon-no-boundary.png')} />

                <TouchableWithoutFeedback onPress={() => this.props.onPressIcon()}>
                    <Image
                        style={styles.rightIcon}
                        source={this.props.icon} />
                </TouchableWithoutFeedback>
            </View>
        );
    }

}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#000',
        height: 56,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    leftIcon: {
        width: 120, 
        maxHeight: 50, 
        resizeMode: 'contain',
        marginHorizontal: 16,
        alignSelf: 'center',
    },
    rightIcon: {
        width: 56, 
        maxHeight: 50,
        alignSelf: 'center',
        resizeMode: 'contain',
        marginHorizontal: 16,
    },
});