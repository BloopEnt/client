import React from 'react';
import { Container, Content } from 'native-base';
import { StyleSheet, Text } from "react-native";

class TextView extends React.Component {
    render() {
        return (
            <Text
                {...this.props}
                style={[styles.textStyle, this.props.style]}>
                {this.props.children}
            </Text>
        );
    }
}

const styles = StyleSheet.create({
    textStyle: {
       // fontFamily: 'Montserrat',
    },
});

export default TextView;