import React from 'react';
import PropTypes from 'prop-types';
import appStyles from "../utils/AppStyles";
import {Container, Content} from 'native-base';
import TextView from "./TextView";


export default class ErrorView extends React.Component {

    static propTypes = {
        message: PropTypes.string.isRequired,
    };

    render() {
        return (
            <Container style={appStyles.errorViewContainer}>
                <Content contentContainerStyle={[appStyles.containerCenter]}>
                    <TextView style={appStyles.errorText}>{this.props.message}</TextView>
                </Content>
            </Container>
        );
    }

}