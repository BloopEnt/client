import React from 'react';
import { StyleSheet, } from "react-native";
import TextView from './TextView';

class TitleTextView extends React.Component {
    render() {
        return (
            <TextView
                {...this.props}
                style={[styles.TitleTextView, this.props.style]}
                overflow={'hidden'}
                numberOfLines={2}>{this.props.children}</TextView>
        );
    }
}

const styles = StyleSheet.create({
    TitleTextView: {
        textAlign: 'center',
        color: 'white',
        fontSize: 23,
    },
});

export default TitleTextView;