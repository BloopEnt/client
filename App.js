import { StackNavigator } from "react-navigation";

import SplashScreen from "./screens/SplashScreen";
import HomeScreen from "./screens/HomeScreen";
import SubCategoriesScreen from "./screens/SubCategoriesScreen";
import GameScreen from "./screens/GameScreen";

import axios from 'axios';
import AppConstants from "./utils/AppConstants";

axios.defaults.baseURL = AppConstants.URL_API_BASE;


export default StackNavigator({

    Splash: {
        screen: SplashScreen,
        navigationOptions: { header: null },
    },

    Home: {
        screen: HomeScreen,
        navigationOptions: { header: null },
    },

    SubCategoryList: {
        screen: SubCategoriesScreen,
        navigationOptions: { header: null },
    },

    Play: {
        screen: GameScreen,
        navigationOptions: { header: null },
    },

}, {
        initialRouteName: 'Splash',
    }
);
