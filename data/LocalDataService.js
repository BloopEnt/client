import {AsyncStorage} from "react-native";

export default class LocalDataService {

    static getString(key: string): Promise<String> {
        return AsyncStorage.getItem(key);
    }

    static saveAsString(key: string, value: any): Promise {
        const valueString = JSON.stringify(value);
        return AsyncStorage.setItem(key, valueString);
    }

    static getBoolean(key: string, fallback: boolean): Promise<Boolean> {
        return AsyncStorage.getItem(key)
            .then(str => {
                // noinspection UnnecessaryLocalVariableJS
                let value = (str && str !== null) ? (str === "1") : fallback;
                // console.log(`getBoolean key: ${key},  raw: ${str},  value: ${value},  fallback: ${fallback}`);
                return value;
            })
            .catch(reason => {
                console.log(`getBoolean key: ${key},  reason: ${reason},  fallback: ${fallback}`);
                return fallback;
            });
    }

    static saveBoolean(key: string, value: boolean): Promise {
        const valueString = value === true ? "1" : "0";
        return AsyncStorage.setItem(key, valueString);
    }

    static getNumber(key: string, fallback: number): Promise<Number> {
        return AsyncStorage.getItem(key)
            .then(str => {
                // noinspection UnnecessaryLocalVariableJS
                let value = (isNaN(str)) ? fallback : Number(str);
                // console.log(`getNumber key: ${key},  raw: ${value},  value: ${value},  fallback: ${fallback}`);
                return value;
            })
            .catch(reason => {
                console.log(`getNumber key: ${key},  reason: ${reason},  fallback: ${fallback}`);
                return fallback;
            });
    }

    static saveNumber(key: string, value: number): Promise {
        const valueString = JSON.stringify(value);
        return AsyncStorage.setItem(key, valueString);
    }

}