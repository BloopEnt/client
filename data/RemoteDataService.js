import axios from "axios/index";
import AppConstants from "../utils/AppConstants";

export default class RemoteDataService {

    static doGet(url: string) {
        return axios.get(url);
    }

}