//@flow

import Category from "../models/Category";
import AppConstants from "../utils/AppConstants";
import DataMapper from "./DataMapper";
import LocalDataService from "./LocalDataService";
import axios from "axios/index";
import SubCategory from "../models/SubCategory";
import Question from "../models/Question";
import RemoteDataService from "./RemoteDataService";
import Dump from "./Dump";
import AppUtils from "../utils/AppUtils";

const readQuestions = [];

function createQuestions(questions) {
    console.log(`questions: ${JSON.stringify(questions)}`);
    console.log(`questions count: ${questions.length}`);

    const read = [];
    const unread = [];

    questions.forEach((question) => {
        if (readQuestions.includes(question.uid)) {
            read.push(question);
        } else {
            unread.push(question);
        }
    })
    console.log(`read questions: ${read.length}`);
    console.log(`unread questions: ${unread.length}`);

    AppUtils.shuffleArray(read);
    AppUtils.shuffleArray(unread);

    const concatenated = [];
    concatenated.push(...unread);
    concatenated.push(...read);

    console.log(`concatenated questions: ${JSON.stringify(concatenated)}`);

    return concatenated;
}

export default class DataRepository {

    static async loadFirstTimeData(): Promise<boolean> {

        try {

            let isFirstTime: boolean = await LocalDataService.getBoolean(AppConstants.KEY_IS_FIRST_TIME, true);
            // console.log('isFirstTime: ' + isFirstTime);
            if (!isFirstTime) Promise.resolve(true);

            const rawCategories: Array<Category> = Dump.getCategories();
            const rawSubcategories: Array<SubCategory> = Dump.getSubCategories();
            const rawQuestions: Array<Question> = Dump.getQuestions();

            // MAP
            const categories: Array<Category> = rawCategories.map(value => DataMapper.parseCategory(value));
            const subcategories: Array<SubCategory> = rawSubcategories.map(value => DataMapper.parseSubcategory(value));
            const questions: Array<Question> = rawQuestions.map(value => DataMapper.parseQuestion(value));


            LocalDataService.saveAsString(AppConstants.KEY_CATEGORIES, categories);
            LocalDataService.saveAsString(AppConstants.KEY_SUBCATEGORIES, subcategories);
            LocalDataService.saveAsString(AppConstants.KEY_QUESTIONS, questions);
            LocalDataService.saveBoolean(AppConstants.KEY_IS_FIRST_TIME, false);

            return Promise.resolve(true);

        } catch (error) {
            console.log(`DataRepository.loadFirstTimeData - error: ${error}`);
            return Promise.reject(error);
        }
    }

    static getLocalDataIfOffline(): Promise<boolean> {

        try {
            const rawCategories: Array<Category> = Dump.getCategories();
            const rawSubcategories: Array<SubCategory> = Dump.getSubCategories();
            const rawQuestions: Array<Question> = Dump.getQuestions();

            // MAP
            const categories: Array<Category> = rawCategories.map(value => DataMapper.parseCategory(value));
            const subcategories: Array<SubCategory> = rawSubcategories.map(value => DataMapper.parseSubcategory(value));
            const questions: Array<Question> = rawQuestions.map(value => DataMapper.parseQuestion(value));


            LocalDataService.saveAsString(AppConstants.KEY_CATEGORIES, categories);
            LocalDataService.saveAsString(AppConstants.KEY_SUBCATEGORIES, subcategories);
            LocalDataService.saveAsString(AppConstants.KEY_QUESTIONS, questions);
            LocalDataService.saveBoolean(AppConstants.KEY_IS_FIRST_TIME, false);

            return Promise.resolve(true);

        } catch (error) {
            console.log(`DataRepository.loadFirstTimeData - error: ${error}`);
            return Promise.reject(error);
        }
    }

    static getAllCategories(): Promise<Array<Category>> {

        return LocalDataService.getString(AppConstants.KEY_CATEGORIES)
            .then(value => {

                if (!value || value == null) {
                    return [];
                }
                let categories: Array<Category> = JSON.parse(value);
                // console.log(`saved categories: ${JSON.stringify(categories)}`);
                // console.log(`saved categories count: ${categories.length}`);

                categories = categories.filter(category => AppUtils.isActive(category.status));
                // console.log(`filtered categories: ${JSON.stringify(categories)}`);
                // console.log(`filtered categories count: ${categories.length}`);

                categories = categories.sort((a, b) => a.index - b.index);
                // console.log(`sorted categories: ${JSON.stringify(categories)}`);

                return categories;
            });
    }

    static getAllSubCategories(): Promise<Array<SubCategory>> {

        return LocalDataService.getString(AppConstants.KEY_SUBCATEGORIES)
            .then(value => {

                if (!value || value == null) {
                    return [];
                }
                const subcategories: Array<SubCategory> = JSON.parse(value);
                // console.log(`saved subcategories: ${JSON.stringify(subcategories)}`);
                return subcategories;
            });
    }

    static getSubCategories(category: Category): Promise<Array<SubCategory>> {
        // console.log(`selected category: ${JSON.stringify(category)}`);

        return LocalDataService.getString(AppConstants.KEY_SUBCATEGORIES)
            .then(value => {

                if (!value || value == null) {
                    return [];
                }

                let items: Array<SubCategory> = JSON.parse(value);
                // console.log(`saved subcategories: ${JSON.stringify(items)}`);
                // console.log(`saved subcategories count: ${items.length}`);

                items = items.filter(subcategory => subcategory.category === category.uid && AppUtils.isActive(subcategory.status));
                // console.log(`filtered subcategories: ${JSON.stringify(items)}`);
                // console.log(`filtered subcategories count: ${items.length}`);

                items = items.sort((a, b) => a.name.localeCompare(b.name));
                // console.log(`sorted subcategories: ${JSON.stringify(items)}`);

                return items;
            });
    }

    static getCustomSubCategories(): Promise<Array<SubCategory>> {

        return LocalDataService.getString(AppConstants.KEY_CUSTOM_SUBCATEGORIES)
            .then(value => {

                if (!value || value == null) {
                    return [];
                }

                let items: Array<SubCategory> = JSON.parse(value);
                // console.log(`saved subcategories: ${JSON.stringify(items)}`);
                // console.log(`saved subcategories count: ${items.length}`);

                items = items.sort((a, b) => a.name.localeCompare(b.name));
                // console.log(`sorted subcategories: ${JSON.stringify(items)}`);

                return items;
            })
            .catch(error => {
                console.log(`getCustomSubCategories - error: ${JSON.stringify(error)}`);
                return [];
            });
    }

    static saveCustomSubCategories(subcategories: Array<SubCategory>) {
        LocalDataService.getString(AppConstants.KEY_CUSTOM_SUBCATEGORIES)
            .then(value => {
                const added: Array<SubCategory> = (value && value != null) ? JSON.parse(value) : [];
                // console.log(`saved custom subcategories count: ${added.length}`);
                // console.log(`saved custom subcategories: ${JSON.stringify(added)}`);

                const filtered = added.filter(subcategory => subcategories.findIndex(v => v.uid === subcategory.uid) === -1);
                // console.log(`filtered custom subcategories count: ${filtered.length}`);
                // console.log(`filtered custom subcategories: ${JSON.stringify(filtered)}`);

                filtered.push(...subcategories);
                // console.log(`push: custom subcategories count: ${filtered.length}`);
                // console.log(`push: custom subcategories: ${JSON.stringify(filtered)}`);

                LocalDataService.saveAsString(AppConstants.KEY_CUSTOM_SUBCATEGORIES, filtered);
            })
            .catch(error => {
                LocalDataService.saveAsString(AppConstants.KEY_CUSTOM_SUBCATEGORIES, subcategories);
            });
    }

    static getCustomQuestions(subcategory: SubCategory): Promise<Array<Question>> {

        return LocalDataService.getString(AppConstants.KEY_CUSTOM_QUESTIONS)
            .then(value => {

                if (!value || value == null) {
                    return [];
                }

                let questions: Array<Question> = JSON.parse(value);
                // console.log(`saved custom questions: ${JSON.stringify(questions)}`);
                // console.log(`saved custom questions count: ${questions.length}`);

                questions = questions.filter(question => question.subcategory === subcategory.uid);
                // console.log(`filtered custom questions: ${JSON.stringify(questions)}`);
                // console.log(`filtered custom questions count: ${questions.length}`);

                return createQuestions(questions);
            })
            .catch(error => {
                console.log(`getCustomQuestions - error: ${JSON.stringify(error)}`);
                return [];
            });
    }

    static saveCustomQuestions(questions: Array<Question>) {

        LocalDataService.getString(AppConstants.KEY_CUSTOM_QUESTIONS)
            .then(value => {

                let alreadyAddedQuestions: Array<Question> = (value && value != null) ? JSON.parse(value) : [];

                // console.log(`saved custom questions count: ${questions.length}`);
                // console.log(`saved custom questions: ${JSON.stringify(questions)}`);

                const filtered = alreadyAddedQuestions.filter(question => questions.findIndex(v => v.uid === question.uid) === -1);

                //  questions = questions.filter(question => questions.findIndex(v => v.uid === question.uid) === -1);
                // console.log(`filtered custom questions count: ${questions.length}`);
                // console.log(`filtered custom questions: ${JSON.stringify(questions)}`);

                filtered.push(...questions);
                //console.log(`push: custom questions count: ${filtered.length}`);
                // console.log(`push: custom questions: ${JSON.stringify(filtered)}`);

                LocalDataService.saveAsString(AppConstants.KEY_CUSTOM_QUESTIONS, filtered);
            })
            .catch(error => {
                LocalDataService.saveAsString(AppConstants.KEY_CUSTOM_QUESTIONS, questions);
            });
    }

    static getQuestions(subcategory: SubCategory): Promise<Array<Question>> {
        // console.log(`selected subcategory: ${JSON.stringify(subcategory)}`);

        return LocalDataService.getString(AppConstants.KEY_QUESTIONS)
            .then(value => {

                if (!value || value == null) {
                    return [];
                }

                let questions: Array<Question> = JSON.parse(value);

                //  console.log(`saved questions: ${questions}`);
                // console.log(`saved questions count: ${questions.length}`);

                questions = questions.filter(question => question.subcategory === subcategory.uid && AppUtils.isActive(question.status));

                // console.log(`filtered questions: ${JSON.stringify(questions)}`);
                // console.log(`filtered questions count: ${questions.length}`);

                return createQuestions(questions);
            })
            .catch(error => {
                console.log(`getQuestions - error: ${JSON.stringify(error)}`);
                return [];
            });
    }

    static getRoundTime(): Promise<Number> {
        return LocalDataService.getNumber(AppConstants.KEY_ROUND_TIME, AppConstants.DEFAULT_ROUND_TIME)
            .then(value => {
                let finalValue = (value === 0) ? AppConstants.DEFAULT_ROUND_TIME : value;
                // console.log(`getRoundTime: response value: ${value}, final value: ${finalValue}`);
                return finalValue;
            });
    }

    static setRoundTime(time: number): Promise<Boolean> {
        return LocalDataService.saveNumber(AppConstants.KEY_ROUND_TIME, time);
    }

    static getSoundEnabled(): Promise<Boolean> {
        return LocalDataService.getBoolean(AppConstants.KEY_SOUND_ENABLED, AppConstants.DEFAULT_SOUND_ENABLED);
    }

    static setSoundEnabled(enabled: boolean): Promise<Boolean> {
        return LocalDataService.saveBoolean(AppConstants.KEY_SOUND_ENABLED, enabled);
    }

    static updateAllData(): Promise<Boolean> {

        return axios.get(AppConstants.URL_API_CATEGORIES)
            .then(response => {

                // RESPONSE
                // console.log(`Response: ${JSON.stringify(response)}`);
                // console.log(`Response status: ${response.status}`);

                if (response.status === 200) {

                    // RAW
                    const rawCategories: Array<any> = response.data.categories;
                    const rawSubcategories: Array<any> = response.data.sub_categories;
                    const rawQuestions: Array<any> = response.data.questions;

                    // console.log(`rawCategories: ${JSON.stringify(rawCategories)}`);
                    // console.log(`rawSubcategories: ${JSON.stringify(rawSubcategories)}`);
                    // console.log(`rawQuestions: ${JSON.stringify(rawQuestions)}`);

                    // MAP
                    const categories: Array<Category> = rawCategories.map(value => DataMapper.parseCategory(value));
                    const subcategories: Array<SubCategory> = rawSubcategories.map(value => DataMapper.parseSubcategory(value));
                    const questions: Array<Question> = rawQuestions.map(value => DataMapper.parseQuestion(value));

                    // console.log(`new categories: ${JSON.stringify(categories)}`);
                    // console.log(`new subcategories: ${JSON.stringify(subcategories)}`);
                    // console.log(`new questions: ${JSON.stringify(questions)}`);

                    // SAVE
                    LocalDataService.saveAsString(AppConstants.KEY_CATEGORIES, categories);
                    LocalDataService.saveAsString(AppConstants.KEY_SUBCATEGORIES, subcategories);
                    LocalDataService.saveAsString(AppConstants.KEY_QUESTIONS, questions);
                }

            });
    }

    static deleteCustomSubCategory(subcategory: SubCategory) {
        return LocalDataService.getString(AppConstants.KEY_CUSTOM_SUBCATEGORIES)
            .then(value => {
                const added: Array<SubCategory> = (value && value != null) ? JSON.parse(value) : [];
                // console.log(`saved custom subcategories count: ${added.length}`);
                // console.log(`saved custom subcategories: ${JSON.stringify(added)}`);

                const filtered = added.filter(item => item.uid !== subcategory.uid);
                console.log(`filtered custom subcategories count: ${filtered.length}`);
                // console.log(`filtered custom subcategories: ${JSON.stringify(filtered)}`);

                LocalDataService.saveAsString(AppConstants.KEY_CUSTOM_SUBCATEGORIES, filtered);
            })
            .catch(error => {
                console.log(`deleteCustomSubCategory: error: ${JSON.stringify(error)}`);
            });
    }

    static deleteCustomQuestions(subcategory: SubCategory) {
        return LocalDataService.getString(AppConstants.KEY_CUSTOM_QUESTIONS)
            .then(value => {

                const added: Array<Question> = (value && value != null) ? JSON.parse(value) : [];
                console.log(`saved custom questions count: ${added.length}`);
                // console.log(`saved custom questions: ${JSON.stringify(added)}`);

                const filtered = added.filter(question => question.subcategory != subcategory.uid);
                console.log(`filtered custom questions count: ${filtered.length}`);
                // console.log(`filtered custom questions: ${JSON.stringify(filtered)}`);

                LocalDataService.saveAsString(AppConstants.KEY_CUSTOM_QUESTIONS, filtered);
            })
            .catch(error => {
                console.log(`deleteCustomQuestions: error: ${JSON.stringify(error)}`);
            });
    }

    static isPurchased(iapId: String): Promise<Boolean> {
        // console.log(`selected category: ${JSON.stringify(category)}`);

        return LocalDataService.getString(AppConstants.KEY_PURCHASES)
            .then(value => {

                if (!value || value == null) {
                    return Promise.resolve(false);
                }

                let items: Array<String> = JSON.parse(value);
                // console.log(`saved purchases: ${JSON.stringify(items)}`);
                // console.log(`saved purchases count: ${items.length}`);

                let contains = items.includes(iapId)
                // console.log(`isPurchased: iapId = ${contains.toString()}`);

                return Promise.resolve(contains);
            });
    }

    static getPurchasedList(): Promise<Array<String>> {
        // console.log(`selected category: ${JSON.stringify(category)}`);

        return LocalDataService.getString(AppConstants.KEY_PURCHASES)
            .then(value => {
                if (!value || value == null) {
                    return Promise.resolve([]);
                }

                let items: Array<String> = JSON.parse(value);
                //console.log(`saved purchases: ${JSON.stringify(items)}`);
                // console.log(`saved purchases count: ${items.length}`);

                return Promise.resolve(items);
            });
    }

    static savePurchases(ids: Array<String>): Promise {

        return LocalDataService.getString(AppConstants.KEY_PURCHASES)
            .then(value => {

                const added: Array<String> = (value && value != null) ? JSON.parse(value) : [];
                // console.log(`saved purchases: ${JSON.stringify(added)}`);
                // console.log(`saved purchases count: ${added.length}`);

                const uniqueItems = new Set([...added, ...ids]);
                // console.log(`purchases to save: ${JSON.stringify(uniqueItems)}`);
                // console.log(`purchases to save count: ${uniqueItems.size}`);

                const uniqueItemsArray = Array.from(uniqueItems);
                // console.log(`saved purchases0: uniqueItemsArray = ${JSON.stringify(uniqueItemsArray)}`);

                LocalDataService.saveAsString(AppConstants.KEY_PURCHASES, uniqueItemsArray);
            })
            .catch(error => {
                LocalDataService.saveAsString(AppConstants.KEY_PURCHASES, ids);
            });
    }

    static markAsRead(question: Question) {
        readQuestions.push(question.uid);
    }
}
