// @flow

import Category from "../models/Category";
import SubCategory from "../models/SubCategory";
import Question from "../models/Question";
import AppConstants from "../utils/AppConstants";
import AppUtils from "../utils/AppUtils";

export default class DataMapper {

    static parseCategory(json: any): Category {

        const uid = json.id;
        const name = json.category_name;
        const background = json.bg_color;
        const icon = json.image;
        const status = json.status;
        const instructions = json.instructions ? json.instructions : "";
        const index = isNaN(json.ordering) ? 0 : parseInt(json.ordering);

        const price = json.price ? json.price : 0;
        const paid = json.paid ? json.paid === "1" : false;
        const iosInAppId = json.ios_app_id ? json.ios_app_id : "";
        const andrdoidInAppId = json.andrdoid_app_id ? json.andrdoid_app_id : "";

        const createTime = json.created_date;
        const updateTime = json.modified_date;

        return new Category(uid, name, createTime, updateTime, icon, background, status, instructions, index, iosInAppId, andrdoidInAppId, price, paid);
    }

    static parseSubcategory(json: any): SubCategory {

        const uid: string = json.id;
        const name: string = json.name;
        const category: string = json.parent_id;
        const background = json.bg_color;
        const status = json.status;

        const createTime: string = json.createTime;
        const updateTime: string = json.updateTime;
        const price = json.price ? json.price : "0";
        const paid = json.paid ? json.paid === "1" : false;
        const iosInAppId: string = json.ios_app_id;
        const andrdoidInAppId: string = json.andrdoid_app_id;
        return new SubCategory(uid, iosInAppId, name, andrdoidInAppId, category, price, paid, createTime, updateTime, background, status);
    }

    static parseQuestion(json: any): Question {

        const uid = json.id;
        const name = json.name;
        const subcategory = json.sub_cat_id;
        const status = json.status;
        const createTime = json.created_date;
        const updateTime = json.updated_date;

        return new Question(uid, name, subcategory, createTime, updateTime, status);
    }

    static createSubcategory(subcategoryName: string): SubCategory {

        const now = new Date().toDateString();

        const backgroundColorIndex = Math.floor( Math.random() * AppConstants.BackgroundColors.length);
        const backgroundColor = AppConstants.BackgroundColors[backgroundColorIndex];

        const uid: string = AppUtils.getUniqueId();
        const name: string = subcategoryName;
        const category: string = AppConstants.CUSTOM_CATEGORY_UID;
        const background = backgroundColor;
        const status = '1';
        const createTime: string = now;
        const updateTime: string = now;

        const price =  "0";
        const paid: boolean = false;
        const iosInAppId: string = '';
        const andrdoidInAppId: string = '';

        return new SubCategory(uid, iosInAppId, name, andrdoidInAppId, category, price, paid, createTime, updateTime, background, status);
    }

    static createQuestion(questionName: string, subCategory: SubCategory): Question {

        const now = new Date().toDateString();

        const uid: string = AppUtils.getUniqueId();
        const name: string = questionName;
        const subcategory = subCategory.uid;
        const status = '1';
        const createTime: string = now;
        const updateTime: string = now;

        return new Question(uid, name, subcategory, createTime, updateTime, status);
    }

}