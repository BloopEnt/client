import DataMapper from "../data/DataMapper";

it('createSubcategory', () => {

    const name = 'test';
    let subcategory = DataMapper.createSubcategory(name);
    console.log(subcategory);

    expect(name).toBe(subcategory.name);
});