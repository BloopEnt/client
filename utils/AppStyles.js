import {Dimensions, StyleSheet,} from 'react-native';

import AppConstants from './AppConstants';

const appStyles = StyleSheet.create({

    containerCenter: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    headerNoDefault: {
        borderBottomWidth: 0,
        paddingTop: 0,
        backgroundColor: 'transparent',
    },

    footerNoDefault: {
        borderTopWidth: 0,
        paddingBottom: 0,
        backgroundColor: 'transparent',
    },

    headerLeftContainerStyle:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },

    headerRightContainerStyle:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    headerItemTextStyle:{
        color: 'white',
        paddingStart: 8,
        fontSize: 15,
    },

    backgroundColored: {
        backgroundColor: AppConstants.COLOR_SKY_BLUE,
    },

    errorViewContainer: {
        backgroundColor: AppConstants.COLOR_RED,
    },

    gridCardStyle: {
        justifyContent: 'center',
        height: AppConstants.DIMEN_GRID_ITEM,
        borderRadius: 0,
    },

    playScreenContainer: {
        flex: 1,
        backgroundColor: AppConstants.COLOR_SKY_BLUE,
    },

    playScreenSuccessContainer: {
        flex: 1,
        backgroundColor: AppConstants.COLOR_GREEN,
    },

    playScreenPassContainer: {
        flex: 1,
        backgroundColor: AppConstants.COLOR_RED,
    },

    playScreenGroupContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    playScreenBackButton: {
        position: 'absolute',
    },

    playScreenQuestionContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    playScreenMessageText: {
        flex: 1,
        position: 'absolute',
        height: AppConstants.Dimensions.WindowHeight,
        width: AppConstants.Dimensions.WindowWidth,
        textAlign: 'center',
        fontSize: 72,
        color: 'white',
    },

    playScreenSubtitle: {
        color: 'white',
        fontSize: 24,
        fontWeight: '500',
        justifyContent: 'center',
        textAlign: 'center',
    },

    playScreenTimeContainer: {
        position: 'absolute',
        height: AppConstants.Dimensions.WindowHeight,
        width: AppConstants.Dimensions.WindowWidth,
        textAlign: 'center',
        fontSize: 22,
        paddingTop: 12,
        color: 'white',
        // backgroundColor: 'red',
    },

    playScreenButtonsContainer: {
        position: 'absolute',
        height: AppConstants.Dimensions.WindowHeight,
        width: AppConstants.Dimensions.WindowWidth,
    },

    playScreenButton: {
        flex: 1,
        borderRadius: 0,
    },

    errorText: {
        color: 'white',
        fontSize: 36,
    }
});


export default appStyles;