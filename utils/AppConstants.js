import {Dimensions} from "react-native";
import Category from "../models/Category";

const fullWidth = Dimensions.get('window').width;
const fullHeight = Dimensions.get('window').height;

//const BASE_URL = 'http://dev.straightupgame.com';
const BASE_URL = 'http://straightupgame.com';
// const BASE_URL = 'http://straightup.skycap.co.in';
// const BASE_URL = 'http://192.168.1.150';

const customCategoryId = 'straightup_custom_category';
const customCategoryInstructions = 'Act Out the actions on the card and No talking';
const customCategoryIcon = '../assets/icons/custom_category_icon.png';

export default AppConstants = {

    Screens: {
        HOME: 'Home',
        SUBCATEGORY_LIST: 'SubCategoryList',
        PLAY_GAME: 'Play',
        GAME_SCORES: 'GameScore',
    },

    Dimensions: {
        WindowWidth: fullWidth,
        WindowHeight: fullHeight,
    },

    BackgroundColors: [
        '#ad7979',
        '#777777',
        '#c99696',
        '#9c3d3d',
        '#378ccc',
        '#9e8181',
        '#7f4949',
        '#2c7f80',
        '#a85050',
        '#324c35',
        '#da1414',
        '#111111',
        '#9a3c1e',
    ],

    DIMEN_GRID_ITEM: 160,

    COLOR_SKY_BLUE: '#0097ed',
    COLOR_RED: '#ff0312',
    COLOR_GREEN: '#009e3a',
    COLOR_GRAY: '#707070',
    COLOR_BLACK_PURE: '#000000',
    COLOR_BLACK: '#171717',
    COLOR_BLACK_LIGHT: '#41403f',
    COLOR_WHITE: '#fff',

    TIME_GET_READY: 5,
    TIME_ANSWER_SMALL: 60,
    TIME_ANSWER_LARGE: 90,
    TIME_ANIMATION: 1500,
    TIME_TILT_INTERVAL: 1000,

    DEFAULT_ROUND_TIME: 60,
    DEFAULT_SOUND_ENABLED: true,
    
    CUSTOM_CATEGORY: new Category(customCategoryId, "Customized Decks", "", "", customCategoryIcon,  "#000000", "1", customCategoryInstructions, 999999999, "", "", 0, false),
    CUSTOM_CATEGORY_UID: customCategoryId,
    CUSTOM_CATEGORY_ICON: require(customCategoryIcon),

    URL_IMAGE_BASE: `${BASE_URL}/assets/uploads/`,
    URL_API_BASE: `${BASE_URL}/index.php/api/straightUp`,
    URL_API_CATEGORIES: `/getCategories`,

    KEY_CATEGORIES: 'categories',
    KEY_SUBCATEGORIES: 'subcategories',
    KEY_CUSTOM_SUBCATEGORIES: 'custom_subcategories',
    KEY_CUSTOM_QUESTIONS: 'custom_questions',
    KEY_QUESTIONS: 'questions',
    KEY_ROUND_TIME: 'round_time',
    KEY_SOUND_ENABLED: 'sound_enabled',
    KEY_IS_FIRST_TIME: 'is_first_time',
    KEY_PURCHASES: 'purchases',

    PLAY_STATE_INITIAL: -1,
    PLAY_STATE_READY: 0,
    PLAY_STATE_PLAYING: 1,
    PLAY_STATE_SUCCESS: 2,
    PLAY_STATE_PASS: 3,
    PLAY_STATE_TIME_UP: 4,
    PLAY_STATE_SCORES: 5,

    SOUND_SPLASH: require('../assets/sounds/independence.mp3'),
    SOUND_COUNTDOWN: require('../assets/sounds/game_countdown.mp3'),
    SOUND_PASS: require('../assets/sounds/buzzer_game_show.wav'),
    SOUND_SUCCESS: require('../assets/sounds/success.wav'),
    SOUND_TIME_UP: require('../assets/sounds/time_bomb_01.mp3'),

    // SOUND_NOTIFICATION: require('../assets/sounds/shiny_notification_dingmp3.mp3'),

    ADMOB_BANNER_IDiOS: 'ca-app-pub-2183429376102879/4583369461',
    ADMOB_INTERSTITIAL_IDiOS: 'ca-app-pub-2183429376102879/9779855828',

    ADMOB_BANNER_IDAndroid: 'ca-app-pub-2183429376102879/5550048340',
    ADMOB_INTERSTITIAL_IDAndroid: 'ca-app-pub-2183429376102879/5905271568',


    // banner test id iOS ca-app-pub-3940256099942544/2934735716
    // interstitial test id iOS ca-app-pub-3940256099942544/4411468910
};
