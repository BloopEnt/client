export default class NumberUtils {

    static round(value) {
        return !value ? 0 : Math.floor(value * 100) / 100;
    }

};