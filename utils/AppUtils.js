import AppConstants from "./AppConstants";
import { Keyboard, } from 'react-native';

export default class AppUtils {

    static isActive(value) {
        return value && value === "1";
    }

    static getImageUrl(imageName) {
        if (imageName === AppConstants.CUSTOM_CATEGORY.icon) {
            return AppConstants.CUSTOM_CATEGORY_ICON;
        }
        return { uri: AppConstants.URL_IMAGE_BASE + imageName };
    }

    static getUniqueId() {
        let nowMillis = new Date().getTime();
        let random = Math.floor(Math.random() * nowMillis);
        return `${nowMillis}-${random}`;
    }

    static hideKeyboard(){
        // console.log('hideKeyboard');
        Keyboard.dismiss();
    }

    static shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }
}