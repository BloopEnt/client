import React from 'react';
import { Dimensions, Image, StatusBar, TouchableWithoutFeedback, View, ScrollView, ActivityIndicator, NetInfo, Platform, } from 'react-native';
import { Button, Container, Content, Icon, } from 'native-base';
import GridView from 'react-native-super-grid';
import { DialogComponent } from 'react-native-dialog-component';

import HeaderView from '../components/HeaderView';
import TextView from '../components/TextView';
import AppConstants from '../utils/AppConstants';
import DataRepository from "../data/DataRepository";
import Category from "../models/Category";
import SubCategory from "../models/SubCategory";
import appStyles from "../utils/AppStyles";
import TitleTextView from '../components/TitleTextView';
import * as RNIap from 'react-native-iap';

const window = Dimensions.get('window');
const height = window.height;
const width = window.width;
const height90pc = window.height * .9;
const width80pc = window.width * .8;
const isLandscape = height > width;

export default class SubCategoriesScreen extends React.Component {

    state = {
        subcategories: [],
        selectedSubcategory: null,

        selectedCategory: AppConstants.CUSTOM_CATEGORY,
        customCategory: false,

        fullWidth: (isLandscape) ? height : width,
        fullHeight: (isLandscape) ? width : height,

        dialogWidth: (isLandscape) ? height90pc : width80pc,
        dialogHeight: (isLandscape) ? width80pc : height90pc,

        isConnected: false,
        purchasedList: [],

    };

    dimHandler = dimens => {
        //console.log('dimensions: ' + JSON.stringify(dimens));

        let height = dimens.window.height;
        let width = dimens.window.width;
        const height90pc = height * .9;
        const width80pc = width * .8;
        let isLandscape = height > width;

        this.setState({
            fullWidth: (isLandscape) ? height : width,
            fullHeight: (isLandscape) ? width : height,
            dialogWidth: (isLandscape) ? height90pc : width80pc,
            dialogHeight: (isLandscape) ? width80pc : height90pc,
        });
    };
    /**
     * Handle internet connection changes
     * */
    internetConnectionStateChange = isConnected => {
        this.setState({ isConnected });
    };

    componentWillMount() {

        const orientationForPlatform = Platform.OS === 'android' ? Expo.ScreenOrientation.Orientation.LANDSCAPE_LEFT : Expo.ScreenOrientation.Orientation.LANDSCAPE_RIGHT;
        Expo.ScreenOrientation.allow(orientationForPlatform);
       
        StatusBar.setHidden(true);
        Dimensions.addEventListener('change', this.dimHandler);
    }

    componentWillUnmount() {
        RNIap.endConnection();
        Dimensions.removeEventListener("change", this.dimHandler);
        NetInfo.isConnected.removeEventListener("connectionChange", this.internetConnectionStateChange);
    }

    componentDidMount() {

        NetInfo.isConnected.fetch().then(isConnected => {
            this.setState({ isConnected });
        });

        NetInfo.isConnected.fetch().then().done(() => {
            NetInfo.isConnected.addEventListener('connectionChange', this.internetConnectionStateChange);
        });
        this.loadInAppPurchase();
        this.loadData();

        DataRepository.getPurchasedList().then(items => {
            this.setState({ purchasedList: items });
        });
    }

    render() {
        return (
            <Container style={{ backgroundColor: AppConstants.COLOR_BLACK_PURE }}>

                <HeaderView
                    icon={require('../assets/icons/house-outline.png')}
                    onPressIcon={() => {
                        this.props.navigation.goBack();
                    }} />

                <GridView
                    items={this.state.subcategories}
                    itemDimension={this.state.fullWidth / 2 - 50}
                    spacing={0}
                    renderItem={(item) =>
                        <TouchableWithoutFeedback onPress={() => {

                            this.setState({ selectedSubcategory: item });
                            const questions = this.state.customCategory ? DataRepository.getCustomQuestions(item) : DataRepository.getQuestions(item);
                            questions.then(value => {
                                if (value.length === 0) {
                                    this.noDataDialogComponent.show();
                                } else if (this.state.customCategory) {
                                    this.gotoPlayScreen();
                                } else {
                                    this.subCategoryClick();
                                }
                            });
                        }}>
                            <View style={{
                                flex: 1,
                                backgroundColor: item.background,
                                alignItems: 'center',
                                justifyContent: 'center',
                                width: this.state.fullWidth / 2,
                                height: AppConstants.DIMEN_GRID_ITEM,
                                padding: 8,
                            }}>

                                {this.checkIfSubCayegoryIsPurchased(item) === false && item.paid === true ? (<Image
                                    source={require('../assets/icons/lock.png')}
                                    ref={ref => { this.imageView = ref; }}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        alignItems: 'flex-start',
                                        marginEnd: 8,
                                        marginTop: 8,
                                        marginBottom: 8,
                                    }}

                                />) : (<Image
                                    // source={require('../assets/icons/lock.png')}
                                    style={{
                                        width: 0,
                                        height: 20,
                                        alignItems: 'flex-start',
                                        marginEnd: 8,
                                        marginTop: 8,
                                        marginBottom: 8,
                                    }}
                                />)}

                                <TitleTextView>{item.name}</TitleTextView>
                            </View>

                        </TouchableWithoutFeedback>
                    }
                />

                {/* DIALOG - HOW-TO-PLAY */}
                <DialogComponent
                    width={(this.state.dialogWidth)}
                    height={(this.state.dialogHeight)}
                    dismissOnHardwareBackPress={true}
                    dismissOnTouchOutside={true}
                    ref={ref => { this.howToPlayDialogComponent = ref; }}>

                    <View style={{ flex: 1, }}>

                        <TouchableWithoutFeedback onPress={() => {
                            this.howToPlayDialogComponent.dismiss();
                        }}>
                            <Image

                                source={require('../assets/icons/close.png')}
                                style={{
                                    width: 20,
                                    height: 20,
                                    alignSelf: 'flex-end',
                                    marginEnd: 8,
                                    marginTop: 8,
                                    marginBottom: 8,
                                }} />
                        </TouchableWithoutFeedback>

                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            paddingHorizontal: 16,
                            justifyContent: 'center',
                        }}>
                            <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                                <TextView style={{
                                    fontSize: 24,
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    color: AppConstants.COLOR_BLACK_LIGHT,
                                }}>{this.state.selectedCategory.instructions}</TextView>
                            </ScrollView>
                        </View>

                        <Button
                            style={{
                                borderRadius: 30,
                                height: 50,
                                paddingStart: 60,
                                paddingEnd: 60,
                                marginVertical: 16,
                                alignSelf: 'center',
                                backgroundColor: AppConstants.COLOR_GREEN,
                            }}
                            onPress={() => {
                                this.howToPlayDialogComponent.dismiss();
                                this.gotoPlayScreen();
                            }}>
                            <TextView style={{ color: 'white', fontSize: 17, }}>Continue</TextView>
                        </Button>

                    </View>
                </DialogComponent>

                {/* DIALOG - NO QUESTION FOUND */}
                <DialogComponent
                    width={460}
                    height={260}
                    dismissOnHardwareBackPress={true}
                    dismissOnTouchOutside={true}
                    ref={(noDataDialogComponent) => {
                        this.noDataDialogComponent = noDataDialogComponent;
                    }}>
                    <View style={{
                        flex: 1,
                    }}>

                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                            <TextView style={{
                                fontSize: 24,
                                textAlign: 'center',
                                padding: 16,
                                color: AppConstants.COLOR_BLACK_LIGHT,
                            }}>
                                Sorry, no question found.
                            </TextView>
                        </View>

                        <Button
                            style={{
                                borderRadius: 30,
                                height: 50,
                                paddingStart: 60,
                                paddingEnd: 60,
                                marginBottom: 26,
                                alignSelf: 'center',
                                backgroundColor: AppConstants.COLOR_GREEN,
                            }}
                            onPress={() => {
                                this.noDataDialogComponent.dismiss();
                            }}>
                            <TextView style={{ color: 'white', fontSize: 17, }}>Dismiss</TextView>
                        </Button>

                    </View>
                </DialogComponent>

                {/* DIALOG - LOADER */}
                <DialogComponent
                    width={120}
                    height={120}
                    dialogStyle={{ borderRadius: 10, overflow: 'hidden', backgroundColor: 'rgba(52,52,52,0)', }}
                    dismissOnHardwareBackPress={false}
                    dismissOnTouchOutside={false}

                    ref={ref => { this.loaderDialogComponent = ref; }}>

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 10, overflow: 'hidden', backgroundColor: 'rgba(52,52,52,0)' }}>
                        <ActivityIndicator size="large" color="#ffffff" />
                    </View>

                </DialogComponent>
            </Container>
        );
    }

    loadData = () => {

        const category: Category = this.props.navigation.state.params.category;
        const customCategory: boolean = this.props.navigation.state.params.customCategory;
        const subCategories = customCategory ? DataRepository.getCustomSubCategories() : DataRepository.getSubCategories(category);

        subCategories.then(subcategories => {
            this.setState({
                selectedCategory: category,
                customCategory: customCategory,
                subcategories: subcategories,
            });
        }).catch(reason => {
            console.log(`error in getting saved subcategories: ${JSON.stringify(reason)}`);
        });
    };

    checkIfSubCayegoryIsPurchased(item: SubCategory) {

        let identifier = (Platform.OS === 'ios') ? item.iosInAppId : item.andrdoidInAppId;

        // console.log(this.state.purchasedList);

        if (this.state.purchasedList.length > 0) {
            let contains = this.state.purchasedList.includes(identifier);
            return contains;
        }
        else {
            return false;
        }
    }

    subCategoryClick = async () => {

        let identifier = '';
        if (this.state.selectedSubcategory.paid) {
            if (Platform.OS === 'ios') {
                identifier = this.state.selectedSubcategory.iosInAppId;
            }
            else {
                identifier = this.state.selectedSubcategory.andrdoidInAppId;
            }
            DataRepository.isPurchased(identifier).then(purchased => {

                if (purchased == false & this.state.isConnected == true) {
                    this.loaderDialogComponent.show();

                    this.buySubCategory(identifier).then(purchaseSucess => {
                        if (purchaseSucess) {

                            const items = [identifier]
                            DataRepository.savePurchases(items).then(saved => {

                                DataRepository.getPurchasedList().then(items => {
                                    this.setState({ purchasedList: items });
                                    this.loadData();
                                }).catch(error => {
                                    // console.log(`error in saving purchases: ${error}`);
                                });
                            }).catch(error => {
                                // console.log(`error in saving purchases: ${error}`);
                            });
                            this.howToPlayDialogComponent.show();


                        }
                    });
                }
                else if (purchased == false & this.state.isConnected == false) {
                    alert(`To see the content of "${this.state.selectedSubcategory.name}" you need to purchase it with active internet connection. Kindly check your internet connection. If you have already purchased it, use Restore Purchases from settings`);
                }
                else {
                    this.howToPlayDialogComponent.show();
                }
            }).catch(error => {
                console.log(error);
            });
        }
        else {
            this.howToPlayDialogComponent.show();
        }
    }

    gotoPlayScreen = () => {
        this.props.navigation.navigate(AppConstants.Screens.PLAY_GAME, {
            subcategory: this.state.selectedSubcategory,
            customCategory: this.state.customCategory,
        });
    }

    async buySubCategory(productIdentifier) {
        const itemSkus = Platform.select({
            ios: [
                productIdentifier
            ],
            android: [
                productIdentifier
            ]
        });

        try {

            //console.log(itemSkus);
            const products = await RNIap.getProducts(itemSkus);
            // console.log(products);

            const purchase = await RNIap.buyProduct(productIdentifier);
            //  console.log(purchase);

            this.loaderDialogComponent.dismiss();
            return true;

        } catch (err) {
            this.loaderDialogComponent.dismiss();
            if (Platform.OS === "android" && err.code == "E_ALREADY_OWNED") {
                return true
            }
            alert(err.message);
            return false;

        }
    }

    // load in app purchase is required for android, sdk already handling os check, so we need not to add any
    loadInAppPurchase = async () => {
        try {
            await RNIap.prepare();

        } catch (err) {
            // console.warn(err);
        }
    }

}