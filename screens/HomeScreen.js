import React from 'react';
import { Dimensions, Image, StatusBar, Text, TouchableWithoutFeedback, View, TextInput, ScrollView, Alert, StyleSheet, Platform, ActivityIndicator, NetInfo, } from 'react-native';
import {
    Body,
    Button,
    Card,
    Container,
    Content,
    Footer,
    FooterTab,
    Header,
    Icon,
    Left,
    List,
    ListItem,
    Right,
    Spinner,
    Title,
} from 'native-base';
import * as Expo from "expo";
import { DialogComponent } from 'react-native-dialog-component';
import { Switch } from 'react-native-switch';
import GridView from 'react-native-super-grid';
import HeaderView from '../components/HeaderView';
import TextView from '../components/TextView';
import CreateButton from '../components/CreateButton';
import AppConstants from '../utils/AppConstants';
import Category from "../models/Category";
import DataRepository from "../data/DataRepository";
import appStyles from "../utils/AppStyles";
import Carousel from "react-native-snap-carousel";
import ToggleView from "../components/ToggleView";
import AppUtils from '../utils/AppUtils';
import DataMapper from '../data/DataMapper';
import SubCategory from '../models/SubCategory';
import TitleTextView from '../components/TitleTextView';
import * as RNIap from 'react-native-iap';



const window = Dimensions.get('window');
const height = window.height;
const width = window.width;
console.log(`dimensions: height: ${height}, width: ${width}`);
const height90pc = window.height * .9;
const width80pc = window.width * .8;
const isLandscape = height > width;
let  isDataLoadedForUpdate = false;

const styles = StyleSheet.create({

    gridItemContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: AppConstants.DIMEN_GRID_ITEM,
        padding: 8,
    },
    gridItemThumbnail: {
        width: 64,
        height: 64,
        marginBottom: 22,
    },
    customDeckButtonContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: AppConstants.COLOR_BLACK_PURE,
        padding: 24,
    },
    customDeckButtonThumbnail: {
        width: 68,
        height: 68,
        marginBottom: 16,
    },
    dialogCrossImage: {
        width: 20,
        height: 20,
        position: 'absolute',
        right: 8,
        top: 8,
    },
    dialogTitle: {
        color: AppConstants.COLOR_BLACK_LIGHT,
        fontSize: 32,
        textAlign: 'center',
        marginBottom: 16,
    },
    dialogSubTitle: {
        alignSelf: 'center',
        marginBottom: 16,
    },
    dialogListContainer: {
        marginHorizontal: 32,
        backgroundColor: AppConstants.COLOR_SKY_BLUE,
        marginBottom: 16,
        borderRadius: 20,
        alignSelf: 'center',
        padding: 16,
    },
    dialogTextInput: {
        marginBottom: 2,
        marginEnd: 4,
        height: 40,
        flex: 1,
        paddingHorizontal: 16,
        backgroundColor: AppConstants.COLOR_WHITE,
        borderRadius: 40,
    },
    dialogAddButtonContainer: {
        width: 38,
        height: 38,
        backgroundColor: AppConstants.COLOR_WHITE,
        borderRadius: 19,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
    },
    dialogBackButtonImage: {
        width: 20,
        height: 20,
    },
    dialogBackButtonText: {
        color: AppConstants.COLOR_BLACK_LIGHT,
        marginLeft: 8,
    },
    dialogAddButtonImage: {
        width: 15,
        height: 15,
    },
    dialogListItemContainer: {
        marginVertical: 2,
        backgroundColor: AppConstants.COLOR_WHITE,
        borderRadius: 400,
    },
    dialogListItemTitle: {
        paddingHorizontal: 16,
        paddingVertical: 8,
        color: AppConstants.COLOR_GRAY,
    },
});

export default class HomeScreen extends React.Component {

    state = {
        categories: [],

        fullWidth: (isLandscape) ? height : width,
        fullHeight: (isLandscape) ? width : height,

        dialogWidth: (isLandscape) ? height90pc : width80pc,
        dialogHeight: (isLandscape) ? width80pc : height90pc,

        // Settings
        roundTime: AppConstants.DEFAULT_ROUND_TIME,
        soundEffects: AppConstants.DEFAULT_SOUND_ENABLED,

        // Tutorials
        tutorialItems: [
            require('../assets/images/tutorial-01.png'),
            require('../assets/images/tutorial-02.png'),
            require('../assets/images/tutorial-03.png'),
            require('../assets/images/tutorial-04.png'),
        ],

        // Add decks
        customSubcategoryName: '',
        customSubcategories: [],

        // Add questions
        customQuestionName: '',
        selectedCustomSubCategory: null,
        customQuestions: [],

        isConnected: false,
    };

    componentWillMount() {

        const orientationForPlatform = Platform.OS === 'android' ? Expo.ScreenOrientation.Orientation.LANDSCAPE_LEFT : Expo.ScreenOrientation.Orientation.LANDSCAPE_RIGHT;
        Expo.ScreenOrientation.allow(orientationForPlatform);

        StatusBar.setHidden(true);
        NetInfo.isConnected.fetch().then().done(() => {
            NetInfo.isConnected.addEventListener('connectionChange', this.internetConnectionStateChange);
        });
        Dimensions.addEventListener('change', this.dimensHandler);
    }

    componentWillUnmount() {
        RNIap.endConnection();
        Dimensions.removeEventListener("change", this.dimensHandler);
        NetInfo.isConnected.removeEventListener("connectionChange", this.internetConnectionStateChange);
    }

    componentDidMount() {

        this.loadInAppPurchase();

        
         DataRepository.loadFirstTimeData()
             .then(value => this.loadData());
        
        // Load custom sub-categories
        // this.loadCustomSubCategories();

        // Load settings
        this.loadSettingsConfig();

        NetInfo.isConnected.fetch().then(isConnected => {
            this.setState({ isConnected });
            if (isDataLoadedForUpdate == false && isConnected == true && Platform.OS === 'android') {
               this.updateDataFromServer();
            }
        });


        //this.loaderDialogComponent.dismiss();

    }

    // load in app purchase is required for android, sdk already handling os check, so we need not to add any
    loadInAppPurchase = async () => {
        try {
            await RNIap.prepare();

        } catch (err) {
            console.warn(err);
        }
    }

    render() {
        return (
            <Container style={{ backgroundColor: AppConstants.COLOR_BLACK_PURE }}>

                {/* HEADER*/}
                <HeaderView
                    icon={require('../assets/icons/ic_menu_white.png')}
                    onPressIcon={() => {
                        this.settingDialogComponent.show();
                    }} />

                {/* CATEGORIES */}
                <Content>
                    <GridView
                        items={this.state.categories}
                        itemDimension={this.state.fullWidth / 2 - 50}
                        spacing={0}
                        renderItem={(item) =>
                            <TouchableWithoutFeedback onPress={() => this.onPressCategory(item)}>
                                <View style={[styles.gridItemContainer, { backgroundColor: item.background, }]}>
                                    <Image
                                        source={AppUtils.getImageUrl(item.icon)}
                                        style={styles.gridItemThumbnail} />
                                    <TitleTextView>{item.name}</TitleTextView>
                                </View>
                            </TouchableWithoutFeedback>
                        }
                    />
                    <TouchableWithoutFeedback onPress={() => { this.addCustomDeckDialogComponent.show(); }}>
                        <View style={styles.customDeckButtonContainer}>
                            <Image
                                style={styles.customDeckButtonThumbnail}
                                source={require('../assets/icons/question_mark.png')} />
                            <TitleTextView>Customize Your Own</TitleTextView>
                        </View>
                    </TouchableWithoutFeedback>
                </Content>

                {/* DIALOG - SETTINGS */}
                <DialogComponent
                    width={460}
                    height={300}
                    dismissOnHardwareBackPress={true}
                    dismissOnTouchOutside={true}
                    ref={ref => { this.settingDialogComponent = ref; }}>

                    <Container>
                        <Content>
                            <TouchableWithoutFeedback onPress={() => {
                                this.settingDialogComponent.dismiss();
                            }}>
                                <View style={{ flex: 1, marginBottom: 8, }}>
                                    <Image
                                        source={require('../assets/icons/close.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            alignSelf: 'flex-end',
                                            marginTop: 8,
                                            marginEnd: 8,
                                            padding: 8,
                                        }} />
                                </View>
                            </TouchableWithoutFeedback>
                            <List>
                                <ListItem style={{ borderBottomWidth: 0, }}>
                                    <Body>
                                        <TextView style={{ fontSize: 19, }}>Round Time</TextView>
                                    </Body>
                                    <Right>
                                        <ToggleView
                                            enabled={this.state.roundTime === AppConstants.TIME_ANSWER_LARGE}
                                            width={100}
                                            height={30}
                                            activeText={`${AppConstants.TIME_ANSWER_LARGE}`}
                                            inactiveText={`${AppConstants.TIME_ANSWER_SMALL}`}
                                            backgroundColor={AppConstants.COLOR_GRAY}
                                            buttonColor={'white'}
                                            onStatusChanged={(enabled) => {
                                                const value = enabled ? AppConstants.TIME_ANSWER_LARGE : AppConstants.TIME_ANSWER_SMALL;
                                                // console.log(`selected round time: ${value}`);
                                                this.setState({ roundTime: value });
                                                this.saveSettingsConfig();
                                            }}
                                        />
                                    </Right>
                                </ListItem>
                                <ListItem style={{ borderBottomWidth: 0, }}>
                                    <Body>
                                        <TextView style={{ fontSize: 19, }}>Sound Effects</TextView>
                                    </Body>
                                    <Right>
                                        <ToggleView
                                            enabled={this.state.soundEffects}
                                            width={100}
                                            height={30}
                                            backgroundColor={AppConstants.COLOR_GRAY}
                                            buttonColor={'white'}
                                            onStatusChanged={(enabled) => {
                                                //console.log(`sound effects enabled: ${enabled}`);
                                                this.setState({ soundEffects: enabled });
                                                this.saveSettingsConfig();
                                            }}
                                        />
                                    </Right>
                                </ListItem>
                                <ListItem style={{ borderBottomWidth: 0, }} onPress={() => {
                                    this.settingDialogComponent.dismiss();
                                    this.loaderDialogComponent.show();
                                    this.restorePurchases();

                                }}>
                                    <TextView style={{ fontSize: 19, }}>Restore Purchases</TextView>
                                </ListItem>
                                <ListItem style={{ borderBottomWidth: 0, }} onPress={() => {
                                    this.settingDialogComponent.dismiss();
                                    this.howToPlayDialogComponent.show();
                                }}>
                                    <TextView style={{ fontSize: 19, }}>How to Play</TextView>
                                </ListItem>
                                <ListItem style={{
                                    borderBottomWidth: 0,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    flex: 1,
                                }}>
                                    <Button
                                        style={{
                                            borderRadius: 30,
                                            height: 50,
                                            paddingStart: 60,
                                            paddingEnd: 60,
                                            alignItems: 'center',
                                            backgroundColor: AppConstants.COLOR_GRAY,
                                        }}
                                        onPress={() => {
                                            this.settingDialogComponent.dismiss();
                                        }}>
                                        <TextView style={{ color: 'white', fontSize: 22, }}>Continue</TextView>
                                    </Button>
                                </ListItem>
                            </List>
                        </Content>
                    </Container>
                </DialogComponent>

                {/* DIALOG - HOW-TO-PLAY */}
                <DialogComponent
                    width={(this.state.dialogWidth)}
                    height={(this.state.dialogHeight)}
                    dismissOnHardwareBackPress={true}
                    dismissOnTouchOutside={true}
                    ref={ref => { this.howToPlayDialogComponent = ref; }}>

                    <View style={{ flex: 1, }}>

                        <Carousel
                            data={this.state.tutorialItems}
                            renderItem={this.renderTutorialItem}
                            sliderWidth={(this.state.dialogWidth)}
                            sliderHeight={(this.state.dialogHeight)}
                            itemWidth={(this.state.dialogWidth)}
                            itemHeight={(this.state.dialogHeight)}
                        />
                        <TouchableWithoutFeedback onPress={() => { this.howToPlayDialogComponent.dismiss(); }}>
                            <Image
                                source={require('../assets/icons/close.png')}
                                style={styles.dialogCrossImage} />
                        </TouchableWithoutFeedback>
                    </View>
                </DialogComponent>

                {/* DIALOG - ADD-CUSTOM-DECK */}
                <DialogComponent
                    width={(this.state.dialogWidth)}
                    height={(this.state.dialogHeight)}
                    dismissOnHardwareBackPress={true}
                    dismissOnTouchOutside={true}
                    onDismissed={AppUtils.hideKeyboard}
                    ref={ref => { this.addCustomDeckDialogComponent = ref; }}>

                    <Container>
                        <Content>

                            {/* HEADER */}
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', }}>
                                <Image style={{ width: 0, height: 20, }} />

                                {/* CROSS */}
                                <TouchableWithoutFeedback onPress={() => { this.addCustomDeckDialogComponent.dismiss(); }}>
                                    <View style={{ padding: 8, }}>
                                        <Image
                                            source={require('../assets/icons/close.png')}
                                            style={{
                                                width: 20,
                                                height: 20,
                                            }} />
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                            <TextView style={styles.dialogTitle}>Create Custom Deck</TextView>

                            {/* LIST/ADD SUBCATEGORIES */}
                            <View style={[styles.dialogListContainer, { width: this.state.dialogWidth - 64, }]}>

                                <TitleTextView style={styles.dialogSubTitle}>Enter Deck Name</TitleTextView>

                                {/* Add deck */}
                                <View style={{ flexDirection: 'row', }}>
                                    <TextInput
                                        style={styles.dialogTextInput}
                                        onSubmitEditing={this.saveSubCategory}
                                        onChangeText={this.onChangeCustomSubcategoryText}
                                        value={this.state.customSubcategoryName}
                                        returnKeyType={'done'}
                                        placeholder={'Deck Name Here'}
                                        underlineColorAndroid='transparent'
                                        blurOnSubmit={false} />
                                    <CreateButton
                                        title={'Create'}
                                        onPress={this.saveSubCategory} />
                                </View>

                                {/* Saved decks */}
                                <List dataArray={this.state.customSubcategories}
                                    renderRow={(subcategory) =>
                                        <TouchableWithoutFeedback onPress={() => this.onPressCustomSubCategory(subcategory)}>
                                            <View style={styles.dialogListItemContainer}>
                                                <TextView style={styles.dialogListItemTitle}>{subcategory.name}</TextView>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    } />
                            </View>

                        </Content>
                    </Container>

                </DialogComponent>

                {/* DIALOG - ADD-CUSTOM-DECK-QUESTIONS */}
                <DialogComponent
                    width={(this.state.dialogWidth)}
                    height={(this.state.dialogHeight)}
                    onDismissed={AppUtils.hideKeyboard}
                    dismissOnHardwareBackPress={true}
                    dismissOnTouchOutside={true}
                    ref={ref => { this.addQuestionsDialogComponent = ref; }}>

                    <Container>
                        <Content>

                            {/* HEADER */}
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', }}>

                                {/* BACK TO LIST */}
                                <TouchableWithoutFeedback onPress={this.closeCustomQuestionDialog}>
                                    <View style={{
                                        padding: 8,
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        // backgroundColor: AppConstants.COLOR_GREEN,
                                    }}>
                                        <Image
                                            source={require('../assets/icons/back-arrow-circular-black.png')}
                                            style={styles.dialogBackButtonImage} />
                                        <TextView style={styles.dialogBackButtonText}>back</TextView>
                                    </View>
                                </TouchableWithoutFeedback>

                                {/* DELETE SUBCATEGORIES */}
                                <TouchableWithoutFeedback onPress={this.confirmDeleteCustomSubCategory}>
                                    <View style={{ padding: 8, }}>
                                        <Image
                                            source={require('../assets/icons/delete_icon.png')}
                                            style={{
                                                width: 20,
                                                height: 20,
                                            }} />
                                    </View>
                                </TouchableWithoutFeedback>

                            </View>

                            <TextView style={styles.dialogTitle}>{this.state.selectedCustomSubCategory && this.state.selectedCustomSubCategory.name}</TextView>

                            {/* LIST/ADD QUESTIONS */}
                            <View style={[styles.dialogListContainer, { width: this.state.dialogWidth - 64, }]}>

                                <TitleTextView style={styles.dialogSubTitle}>Enter Word</TitleTextView>

                                {/* Add deck */}
                                <View style={{ flexDirection: 'row', }}>
                                    <TextInput
                                        style={styles.dialogTextInput}
                                        onSubmitEditing={this.saveCustomQuestion}
                                        onChangeText={this.onChangeCustomQuestionText}
                                        value={this.state.customQuestionName}
                                        underlineColorAndroid='transparent'
                                        blurOnSubmit={false}
                                        returnKeyType={'done'}
                                        placeholder={'Enter Here'} />
                                    <CreateButton
                                        title={'Create'}
                                        onPress={this.saveCustomQuestion} />
                                </View>

                                {/* Saved questions */}
                                <List
                                    dataArray={this.state.customQuestions}
                                    renderRow={(question) =>
                                        <View style={styles.dialogListItemContainer}>
                                            <TextView style={styles.dialogListItemTitle}>{question.name}</TextView>
                                        </View>
                                    }
                                />
                            </View>

                        </Content>
                    </Container>

                </DialogComponent>

                {/* DIALOG - NO SUB-CATEGORY FOUND */}
                <DialogComponent
                    width={460}
                    height={260}
                    dismissOnHardwareBackPress={true}
                    dismissOnTouchOutside={true}
                    ref={ref => { this.noSubCategoriesDialogComponent = ref; }}>

                    <View style={{ flex: 1, }}>

                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                            <TextView style={{
                                fontSize: 24,
                                textAlign: 'center',
                                padding: 16,
                                color: AppConstants.COLOR_BLACK_LIGHT,
                            }}>
                                Sorry, no subcategory found.
                            </TextView>
                        </View>

                        <Button
                            style={{
                                borderRadius: 30,
                                height: 50,
                                paddingStart: 60,
                                paddingEnd: 60,
                                marginBottom: 26,
                                alignSelf: 'center',
                                backgroundColor: AppConstants.COLOR_GREEN,
                            }}
                            onPress={() => {
                                this.noSubCategoriesDialogComponent.dismiss();
                            }}>
                            <TextView style={{ color: 'white', fontSize: 17, }}>Dismiss</TextView>
                        </Button>

                    </View>
                </DialogComponent>

                {/* DIALOG - LOADER */}
                <DialogComponent
                    width={120}
                    height={120}
                    dialogStyle={{ borderRadius: 10, overflow: 'hidden', backgroundColor: 'rgba(52,52,52,0)', }}
                    dismissOnHardwareBackPress={false}
                    dismissOnTouchOutside={false}

                    ref={ref => { this.loaderDialogComponent = ref; }}>

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 10, overflow: 'hidden', backgroundColor: 'rgba(52,52,52,0)' }}>
                        <ActivityIndicator size="large" color="#ffffff" />
                    </View>

                </DialogComponent>
            </Container>
        );
    }

    /**
     * Handle dimensions changes
     * */
    dimensHandler = dimens => {
        let height = dimens.window.height;
        let width = dimens.window.width;

        console.log(`dimensions: height: ${height}, width: ${width}`);

        const height90pc = height * .9;
        const width80pc = width * .8;
        let isLandscape = height > width;

        this.setState({
            fullWidth: (isLandscape) ? height : width,
            fullHeight: (isLandscape) ? width : height,

            dialogWidth: (isLandscape) ? height90pc : width80pc,
            dialogHeight: (isLandscape) ? width80pc : height90pc,
        });
    };

    /**
     * Handle internet connection changes
     * */
    internetConnectionStateChange = isConnected => {
        this.setState({ isConnected });
        if (isDataLoadedForUpdate == false && isConnected == true) {
           this.updateDataFromServer();
        }
    };

    updateDataFromServer = () => {
        isDataLoadedForUpdate = true;
        DataRepository.updateAllData()
            .then(() => this.loadData())
            .catch((error) => console.log(`error in getting data from server: ${error.message}`));
    };
    /**
     * Load local data
     * */
    loadData = () => {
        DataRepository.getAllCategories()
            .then(categories => {
                this.setState({
                    error: false,
                    categories: categories,
                });

                //console.log(categories);

                this.addCustomCategoryToGrid();
                this.loadCustomSubCategories();

            })
            .catch(reason => console.log(`error - load saved categories, reason: ${JSON.stringify(reason)}`));
    };

    /**
     * Load settings dialog configurations
     * */
    loadSettingsConfig = () => {

        DataRepository.getRoundTime()
            .then(time => {
                // console.log('round time: ' + time);

                this.setState({
                    roundTime: time,
                });
            });

        DataRepository.getSoundEnabled()
            .then(enabled => {
                // console.log('sound enabled: ' + enabled);

                this.setState({
                    soundEffects: enabled,
                });
            });
    };

    /**
     * Save settings configurations
     * */
    saveSettingsConfig = () => {

        DataRepository.setRoundTime(this.state.roundTime)
            .then(() => {
                // console.log('saved round time: ' + this.state.roundTime);
            });

        DataRepository.setSoundEnabled(this.state.soundEffects)
            .then(() => {
                // console.log('saved sound settings: ' + this.state.soundEffects);
            });
    };

    /**
     * Create view for tutorial
     * */
    renderTutorialItem = ({ item, index }) => {
        return (
            <View>
                <Image
                    source={item}
                    style={{
                        width: (this.state.dialogWidth),
                        height: (this.state.dialogHeight),
                        resizeMode: 'contain',
                    }}
                />
            </View>
        );
    }

    onChangeCustomSubcategoryText = (text) => {
        this.setState({ customSubcategoryName: text });
    }

    saveSubCategory = () => {
        let name = this.state.customSubcategoryName.trim();
        let notEmpty = name.length > 0;

       console.log('saveSubCategory');

        if (notEmpty) {

            console.log ("notEmpty");

            let alreadyAdded = this.state.customSubcategories.findIndex(value => name.toLowerCase() === value.name.toLowerCase()) !== -1;

            if (alreadyAdded) {
                Alert.alert(
                    'Error',
                    `${name} already added!`,
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                    { cancelable: false }
                );
                return;
            }

            this.setState(prevState => {
                let { customSubcategories, customSubcategoryName } = prevState;
                let customSubcategory = DataMapper.createSubcategory(customSubcategoryName);
                return {
                    customSubcategories: [customSubcategory, ...customSubcategories],
                    customSubcategoryName: '',
                };
            }, () => {
                DataRepository.saveCustomSubCategories(this.state.customSubcategories);
                this.addCustomCategoryToGrid();
            });
        }
    };

    /**
     * add custom category
     */
    addCustomCategoryToGrid = () => {
        

        const haveSubCategories = this.state.customSubcategories.length > 0;
        // console.log(`haveSubCategories: ${haveSubCategories}`);

        const haveCategories = this.state.categories.length > 0;
        // console.log(`haveCategories: ${haveCategories}`);

        const customCategory = AppConstants.CUSTOM_CATEGORY;

        if (haveSubCategories) {
            let pushCategory = false;

            if (haveCategories) {
                const lastIndex = this.state.categories.length - 1;
                // console.log(`lastIndex: ${lastIndex}`);

                const lastCategory: Category = this.state.categories[lastIndex];
                // console.log(`lastCategory: ${JSON.stringify(lastCategory)}`);

                if (lastCategory.uid !== customCategory.uid) {
                    pushCategory = true;
                }
            } else {
                pushCategory = true;
            }

            if (pushCategory) {
                // console.log(`pushCategory: ${pushCategory}`);
                this.state.categories.push(customCategory);

                this.setState({
                    categories: this.state.categories,
                });
            }

        } else if (haveCategories) {

            const lastIndex = this.state.categories.length - 1;
            // console.log(`lastIndex: ${lastIndex}`);

            const lastCategory: Category = this.state.categories[lastIndex];
            //console.log(`lastCategory: ${JSON.stringify(lastCategory)}`);

            if (lastCategory.uid === customCategory.uid) {
                this.state.categories.pop();

                this.setState({
                    categories: this.state.categories,
                });
            }
        }
    };

    onPressCustomSubCategory = (subcategory: SubCategory) => {
        
        console.log('subcategory')

        DataRepository.getCustomQuestions(subcategory)
            .then(questions => {

                this.setState({
                    selectedCustomSubCategory: subcategory,
                    customQuestions: questions,
                }, () => {
                    this.addCustomDeckDialogComponent.dismiss();
                    this.addQuestionsDialogComponent.show();
                });
            });
    };

    onChangeCustomQuestionText = (text) => {
        this.setState({ customQuestionName: text });
    }

    saveCustomQuestion = () => {
        let name = this.state.customQuestionName.trim();
        let notEmpty = name.length > 0;

        if (notEmpty) {
            let alreadyAdded = this.state.customQuestions.findIndex(value => name.toLowerCase() === value.name.toLowerCase()) !== -1;

            if (alreadyAdded) {
                Alert.alert(
                    'Error',
                    `${name} already added!`,
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                    { cancelable: false }
                );
                return;
            }

            this.setState(prevState => {
                let { customQuestions, customQuestionName, selectedCustomSubCategory } = prevState;
                let customQuestion = DataMapper.createQuestion(customQuestionName, selectedCustomSubCategory);

                return {
                    customQuestions: [customQuestion, ...customQuestions],
                    customQuestionName: '',
                };
            }, () => {
                DataRepository.saveCustomQuestions(this.state.customQuestions);
            });
        }
    };

    confirmDeleteCustomSubCategory = () => {
        const { selectedCustomSubCategory } = this.state;

        if (selectedCustomSubCategory && selectedCustomSubCategory != null) {
            Alert.alert(
                'Warning',
                `Do you really want to delete ${selectedCustomSubCategory.name}?`,
                [
                    { text: 'YES', onPress: () => { this.deleteCustomSubCategory(selectedCustomSubCategory) } },
                    { text: 'CANCEL', onPress: () => { } },
                ],
                { cancelable: false }
            );
        }
    };

    deleteCustomSubCategory = (subcategory: SubCategory) => {
        DataRepository.deleteCustomSubCategory(subcategory)
            .then(() => {

                DataRepository.deleteCustomQuestions(subcategory)
                    .then(() => {

                        this.loadCustomSubCategories();
                        this.closeCustomQuestionDialog();
                    });
            });
    };

    loadCustomSubCategories = () => {
        DataRepository.getCustomSubCategories()
            .then(subcategories => {
                // console.log(`saved custom subcategories count: ${subcategories.length}`);
                // console.log(`saved custom subcategories: ${JSON.stringify(subcategories)}`);
                this.setState({
                    customSubcategories: subcategories,
                }, () => {
                    this.addCustomCategoryToGrid();
                });
            })
            .catch(error => {
                console.log(`error in getting custom saved subcategories: ${error}`);
            });
    };

    closeCustomQuestionDialog = () => {
        this.addQuestionsDialogComponent.dismiss();
        this.addCustomDeckDialogComponent.show();
    };

    onPressCategory = (category) => {

        console.log(`clicked: ${category.name}`);

        // let identifier = '';
        // if (category.paid) {
        //     if (Platform.OS === 'ios') {
        //         identifier = category.iosInAppId;
        //     }
        //     else {
        //         identifier = category.andrdoidInAppId;
        //     }
        //     // console.log(internetConnection);

        //     DataRepository.isPurchased(identifier).then(purchased => {
        //         console.log(`isPurchased: ${identifier} = ${purchased}`);

        //         if (purchased == false & this.state.isConnected == true) {
        //             this.loaderDialogComponent.show();
        //             this.buyCategory(identifier).then(purchaseSucess => {
        //                 if (purchaseSucess) {
        //                     const items = [identifier]
        //                     DataRepository.savePurchases(items).then(saved => {
        //                         // console.log(`purchases saved: ${items}`);
        //                     })
        //                         .catch(error => {
        //                             // console.log(`error in saving purchases: ${error}`);
        //                         });
        //                     this.moveToSubCategoriesScreen(category);
        //                 }
        //             });
        //         }
        //         else if (purchased == false & this.state.isConnected == false) {
        //             alert(`To see the content of "${category.name}" you need to purchase it with active internet connection. Kindly check your internet connection. If you have already purchased it, use Restore Purchases from settings`);
        //         }
        //         else {
        //             this.moveToSubCategoriesScreen(category);
        //         }
        //     })
        //         .catch(error => {
        //             console.log(`error in getting saved purchase: ${iapId} = ${error}`);
        //         });
        // }
        // else {
            this.moveToSubCategoriesScreen(category);
       // }
    };

    moveToSubCategoriesScreen(category) {

        const customCategory = category.uid === AppConstants.CUSTOM_CATEGORY_UID;
        const subCategories = customCategory ? DataRepository.getCustomSubCategories() : DataRepository.getSubCategories(category);

        subCategories.then(value => {

            if (value.length === 0) {
                this.noSubCategoriesDialogComponent.show();

            } else {
                this.props.navigation.navigate(AppConstants.Screens.SUBCATEGORY_LIST, {
                    category: category,
                    customCategory: customCategory,
                });
            }
        });
    };

    async buyCategory(productIdentifier) {

        console.log(productIdentifier);
        const itemSkus = Platform.select({
            ios: [
                productIdentifier
            ],
            android: [
                productIdentifier
            ]
        });
        try {
            const products = await RNIap.getProducts(itemSkus);
            console.log(products);

            const purchase = await RNIap.buyProduct(productIdentifier);
            console.log(purchase);
            this.loaderDialogComponent.dismiss();
            return true;

        } catch (err) {
            this.loaderDialogComponent.dismiss();
            alert(err.message);
            return false;
            console.warn(err);
        }
    };

    restorePurchases = async () => {
        try {
            const purchases = await RNIap.getAvailablePurchases();
            // console.log(purchases);
            let arrPurchases = [];
            purchases.forEach(purchase => {
                arrPurchases.push(purchase.productId);
            })
            DataRepository.savePurchases(arrPurchases);
            this.loaderDialogComponent.dismiss();
            Alert.alert(
                'Restore Successful',
                `You successfully restored your purchases`,
                [
                    { text: 'OK', onPress: () => { } },
                ],
                { cancelable: true }
            );
        } catch (err) {
            this.loaderDialogComponent.dismiss();
            console.warn(err); // standardized err.code and err.message available
            alert(err.message);
        }
    }
};





