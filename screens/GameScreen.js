import React from 'react';
import { Dimensions, Image, StatusBar, TouchableWithoutFeedback, View, Platform, } from 'react-native';
import { Container, Content, } from 'native-base';
import Expo, { Accelerometer, AdMobBanner, AdMobInterstitial, } from 'expo';
import GridView from 'react-native-super-grid';

import appStyles from "../utils/AppStyles";
import AppConstants from '../utils/AppConstants';
import NumberUtils from '../utils/NumberUtils';
import SubCategory from "../models/SubCategory";
import DataRepository from "../data/DataRepository";
import TextView from "../components/TextView";

const window = Dimensions.get('window');
const height = window.height;
const width = window.width;
const isLandscape = height > width;

const passSound = new Expo.Audio.Sound();
const successSound = new Expo.Audio.Sound();
const countdownSound = new Expo.Audio.Sound();
const timesUpSound = new Expo.Audio.Sound();

export default class GameScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            playState: AppConstants.PLAY_STATE_INITIAL,

            question: "",
            timeRemaining: "",

            roundTime: AppConstants.DEFAULT_ROUND_TIME,
            soundEffects: AppConstants.DEFAULT_SOUND_ENABLED,

            fullWidth: (isLandscape) ? height : width,
            fullHeight: (isLandscape) ? width : height,

            questions: [],
            answers: [],

            selectedIndex: -1,

            isStraight: null,
            isTiltErrorVisible: false,

            tiltErrorVisibleTimer: null,
            questionTimeoutReference: null,
            getReadyTimeoutReference: null,
            animationTimeoutReference: null,
        };
    }

    componentWillMount() {

        const orientationForPlatform = Platform.OS === 'android' ? Expo.ScreenOrientation.Orientation.LANDSCAPE_LEFT : Expo.ScreenOrientation.Orientation.LANDSCAPE_RIGHT;
        Expo.ScreenOrientation.allow(orientationForPlatform);

        StatusBar.setHidden(true);
        Dimensions.addEventListener('change', this.dimHandler);

            DataRepository.getRoundTime()          
            .then(roundTime => {
                this.setState({
                    roundTime: roundTime,
                });
            });

        DataRepository.getSoundEnabled()
            .then(enabled => {
                this.setState({
                    soundEffects: enabled,
                });
            });

        const subcategory: SubCategory = this.props.navigation.state.params.subcategory;
        const customCategory: boolean = this.props.navigation.state.params.customCategory;
        const questions = customCategory ? DataRepository.getCustomQuestions(subcategory) : DataRepository.getQuestions(subcategory);

        questions.then(questions => {
            this.setState({
                questions: questions,
            });
        });

    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.dimHandler);

        this.stopGetReadyTimer();
        this.stopQuestionTimer();
        this.unsubscribeAccelerometer();
        this.stopTiltErrorVisibleTimer();
        clearTimeout(this.state.animationTimeoutReference);
    }

    async componentDidMount() {

        // Load sounds
        const { isLoaded } = await countdownSound.getStatusAsync();
        if (!isLoaded) {
            await countdownSound.loadAsync(AppConstants.SOUND_COUNTDOWN);
            await successSound.loadAsync(AppConstants.SOUND_SUCCESS);
            await passSound.loadAsync(AppConstants.SOUND_PASS);
            await timesUpSound.loadAsync(AppConstants.SOUND_TIME_UP);
        }

        // Subscribe tilt changes
        this.subscribeAccelerometer();

        // TODO: comment after test
        // this.showFullScreenAd();
    }

    render() {
        // if (true) {
        if (this.state.playState === AppConstants.PLAY_STATE_SCORES) {
            return (
                <Container style={{
                    flex: 1,
                    backgroundColor: `${this.getBackgroundColor(this.state.playState)}`,
                }}>

                    {/* HEADER */}
                    <View style={{
                        height: 50,
                        paddingHorizontal: 16,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        backgroundColor: AppConstants.COLOR_BLACK,
                    }}>

                        <TouchableWithoutFeedback onPress={this.onBackClick}>

                            <View style={{
                                height: 50,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                                // backgroundColor: AppConstants.COLOR_GREEN,
                            }}>

                                <Image
                                    style={{ width: 34, height: 34, resizeMode: 'contain', }}
                                    source={require('../assets/icons/back-arrow-circular-white.png')} />

                                <TextView style={appStyles.headerItemTextStyle}>back</TextView>

                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: 'center',
                            // backgroundColor: AppConstants.COLOR_GREEN,
                        }}>

                            <TextView style={{ fontSize: 35, color: 'white', paddingHorizontal: 8, }}>FINAL</TextView>
                            <View style={{ width: 108, }} />
                            <TextView style={{ fontSize: 35, color: 'white', paddingHorizontal: 8, }}>SCORE</TextView>

                        </View>

                        <TouchableWithoutFeedback onPress={this.playAgain}>

                            <View style={appStyles.headerRightContainerStyle}>

                                <TextView style={appStyles.headerItemTextStyle}>play</TextView>

                                <Image
                                    style={{ marginStart: 8, width: 34, height: 34, resizeMode: 'contain', }}
                                    source={require('../assets/icons/play-button.png')} />

                            </View>
                        </TouchableWithoutFeedback>

                    </View>

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        // backgroundColor: AppConstants.COLOR_GREEN,
                    }}>

                        <View style={{
                            width: 108,
                            height: 108,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: AppConstants.COLOR_BLACK,
                            borderColor: AppConstants.COLOR_SKY_BLUE,
                            borderTopWidth: 6,
                            borderBottomWidth: 6,
                            borderStartWidth: 6,
                            borderEndWidth: 6,
                            borderRadius: 54,
                            marginTop: -30,
                        }}>
                            <TextView style={{ fontSize: 35, color: 'white', }}>
                                {this.state.answers.filter(answer => answer.answered === true).length}
                            </TextView>
                        </View>

                    </View>

                    {/* CONTENTS */}
                    <Content style={{
                        marginTop: 5,
                        // backgroundColor: AppConstants.COLOR_GRAY, minHeight: 100,
                    }}>

                        <GridView
                            items={this.state.answers}
                            itemDimension={this.state.fullWidth / 2 - 50}
                            spacing={0}
                            renderItem={(item) =>
                                <View>
                                    <TextView
                                        style={{
                                            paddingHorizontal: 24,
                                            paddingBottom: 24,
                                            fontSize: 25,
                                            textAlign: 'center',
                                            color: this.getAnswerColor(item)
                                        }}>
                                        {item.question.name}
                                    </TextView>
                                </View>
                            }
                        />
                        <View style={{ height: 10, }} />
                    </Content>

                </Container>
            );

        } else {
            return (
                <Container style={{
                    flex: 1,
                    backgroundColor: `${this.getBackgroundColor(this.state.playState)}`,
                }}>

                    {/* HEADER */}
                    <View style={{
                        height: 50,
                        flexDirection: 'row',
                        paddingHorizontal: 40,
                        justifyContent: 'space-between',
                        // backgroundColor: AppConstants.COLOR_RED,
                    }}>

                        <TouchableWithoutFeedback onPress={this.onBackClick}>

                            <View style={{
                                height: 50,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                                // backgroundColor: AppConstants.COLOR_GREEN,
                            }}>

                                <Image
                                    style={{ width: 34, height: 34, resizeMode: 'contain', }}
                                    source={require('../assets/icons/back-arrow-circular-white.png')} />

                                <TextView style={appStyles.headerItemTextStyle}>back</TextView>

                            </View>
                        </TouchableWithoutFeedback>

                        <View>

                            <View style={{
                                flexDirection: 'row',
                                height: 50,
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                display: this.isClockVisible(this.state.playState) ? 'flex' : 'none',
                                // backgroundColor: AppConstants.COLOR_GREEN,
                            }}>

                                <Image
                                    style={{ width: 34, height: 34, resizeMode: 'contain', }}
                                    source={require('../assets/icons/stopwatch.png')} />

                                <TextView
                                    style={[appStyles.headerItemTextStyle, { minWidth: 45, }]}>
                                    {this.state.timeRemaining}
                                </TextView>

                            </View>
                        </View>

                    </View>

                    {/* CONTENTS */}
                    <Content contentContainerStyle={[appStyles.containerCenter, {/* backgroundColor: AppConstants.COLOR_RED,*/ }]}>
                        <Image
                            source={this.getContentIcon(this.state.playState)}
                            style={{
                                width: 72,
                                height: 72,
                                resizeMode: 'contain',
                                display: this.displayContentIcon(this.state.playState),
                            }} />

                        <TextView style={{
                            color: 'white',
                            fontSize: 50,
                            justifyContent: 'center',
                            textAlign: 'center',
                            paddingHorizontal: 40,
                        }}>
                            {this.getContent(this.state.playState)}
                        </TextView>

                        {/* SUCCESS/PASS BUTTONS */}
                        <View style={{
                            position: 'absolute',
                            height: this.state.fullHeight,
                            width: this.state.fullWidth,
                            display: (this.state.isStraight ? 'flex' : 'none'),
                            flex: 1,
                            flexDirection: 'row',
                            // backgroundColor: AppConstants.COLOR_RED,
                        }}>
                            <TouchableWithoutFeedback onPress={this.onClickPass}>
                                <View style={[appStyles.playScreenButton, {/* backgroundColor: AppConstants.COLOR_RED,*/ }]} />
                            </TouchableWithoutFeedback>

                            <TouchableWithoutFeedback onPress={this.onClickSuccess}>
                                <View style={[appStyles.playScreenButton, {/* backgroundColor: AppConstants.COLOR_GREEN, */ }]} />
                            </TouchableWithoutFeedback>
                        </View>

                    </Content>

                    {/* FOOTER */}
                    <View style={[appStyles.footerNoDefault, {
                        display: this.isFooterVisible(this.state.playState) ? 'flex' : 'none'
                    }]}>
                        <TextView style={{
                            color: 'white',
                            fontSize: 24,
                            marginBottom: 10,
                            justifyContent: 'center',
                            textAlign: 'center',
                        }}>Hold device upright to continue</TextView>
                    </View>

                    {/* ADS */}
                    <View style={{ alignItems: 'center', }}>
                        <AdMobBanner
                            bannerSize="fullBanner"
                            adUnitID={this.isRunningOniOSDevice()}
                            onDidFailToReceiveAdWithError={error => this.bannerError(error)}
                        />
                    </View>

                </Container>
            );
        }
    }

    bannerError(error) {
        console.log(error);
        return;
    }

    dimHandler = dimens => {
        //console.log('dimensions: ' + JSON.stringify(dimens));

        let height = dimens.window.height;
        let width = dimens.window.width;
        let isLandscape = height > width;

        this.setState({
            fullWidth: (isLandscape) ? height : width,
            fullHeight: (isLandscape) ? width : height,
        });
    };

    /**
     * Subscribe tilt changes
     * */
    subscribeAccelerometer = async () => {
        console.log('subscribeAccelerometer');

        this.subscription = Accelerometer.addListener(async (accelerometerData) => {

            let z = NumberUtils.round(accelerometerData.z);
            // console.log(`z value: ${z}`);

            const isTiltUp = Platform.OS === 'android' ? z == 0.0 : z < 0.42;
            const isTiltDown = Platform.OS === 'android' ? z == 0.0 : z > -0.42;


            const isStraight = (isTiltUp && isTiltDown);
            const wasStraight = this.state.isStraight;

            console.log(`isStraight: ${isStraight}, wasStraight: ${wasStraight}, isTiltUp: ${isTiltUp}, isTiltDown: ${isTiltDown}`);

            this.setState({
                isStraight: isStraight,
            });

            if (isStraight) {

                // start timer
                if (this.state.playState === AppConstants.PLAY_STATE_INITIAL) {
                    console.log(`subscribeAccelerometer - startGetReadyTimer`);
                    this.startGetReadyTimer();
                    return;
                }

                // select next question
                if (!wasStraight && (this.state.playState === AppConstants.PLAY_STATE_PASS || this.state.playState === AppConstants.PLAY_STATE_SUCCESS)) {
                    console.log('subscribeAccelerometer - selectNextQuestion()');
                    this.selectNextQuestion();
                }

                // stop error timer
                this.stopTiltErrorVisibleTimer();
                this.setState({
                    isTiltErrorVisible: false,
                });
            }

            // tilt based operations
            if (this.state.playState === AppConstants.PLAY_STATE_PLAYING) {

                if (!isStraight && wasStraight !== isStraight) {

                    if (isTiltUp) {
                        console.log('subscribeAccelerometer - createAnswer(false)');

                        const answer = this.createAnswer(false);

                        this.setState({
                            playState: AppConstants.PLAY_STATE_PASS,
                            answers: [...this.state.answers, answer]
                        });

                        if (this.state.soundEffects) {
                            await passSound.setPositionAsync(0);
                            await passSound.playAsync();
                        }

                        this.startTiltErrorVisibleTimer();

                    } else if (isTiltDown) {
                        console.log('subscribeAccelerometer - createAnswer(true)');

                        const answer = this.createAnswer(true);
                        this.setState({
                            playState: AppConstants.PLAY_STATE_SUCCESS,
                            answers: [...this.state.answers, answer]
                        });

                        if (this.state.soundEffects) {
                            await successSound.setPositionAsync(0);
                            await successSound.playAsync();
                        }

                        this.startTiltErrorVisibleTimer();
                    }
                }
            }
        });

        Accelerometer.setUpdateInterval(AppConstants.TIME_TILT_INTERVAL);
    };

    unsubscribeAccelerometer = () => {
        this.subscription && this.subscription.remove();
        Accelerometer.removeAllListeners();
    };

    selectNextQuestion = () => {
        console.log('selectNextQuestion');

        if (this.state.timeRemaining <= 1) {
            console.log('selectNextQuestion - timeRemaining <= 1');
            return;
        }

        const questionsLength = this.state.questions.length;
        let index = this.state.selectedIndex;
        index = (index === questionsLength - 1) ? 0 : index + 1;
        const question = this.state.questions[index].name;

        this.setState({
            playState: AppConstants.PLAY_STATE_PLAYING,
            selectedIndex: index,
            question: question,
        });

    };

    onClickSuccess = async () => {
        console.log('onClickSuccess');

        if (!this.state.isStraight || this.state.playState !== AppConstants.PLAY_STATE_PLAYING) {
            return;
        }

        const animationTimeoutReference = setTimeout(() => {
            console.log('onClickSuccess - selectNextQuestion()');
            this.selectNextQuestion();
        }, AppConstants.TIME_ANIMATION);

        const answer = this.createAnswer(true);

        this.setState({
            playState: AppConstants.PLAY_STATE_SUCCESS,
            animationTimeoutReference: animationTimeoutReference,
            answers: [...this.state.answers, answer]
        });

        if (this.state.soundEffects) {
            await successSound.setPositionAsync(0);
            await successSound.playAsync();
        }
    };

    onClickPass = async () => {
        console.log('onClickPass');

        if (!this.state.isStraight || this.state.playState !== AppConstants.PLAY_STATE_PLAYING) {
            return;
        }

        const animationTimeoutReference = setTimeout(() => {
            console.log('onClickPass - selectNextQuestion()');
            this.selectNextQuestion();
        }, AppConstants.TIME_ANIMATION);

        const answer = this.createAnswer(false);

        this.setState({
            playState: AppConstants.PLAY_STATE_PASS,
            animationTimeoutReference: animationTimeoutReference,
            answers: [...this.state.answers, answer]
        });

        if (this.state.soundEffects) {
            await passSound.setPositionAsync(0);
            await passSound.playAsync();
        }
    };

    onBackClick = () => {
        console.log('_onBackClick');
        this.props.navigation.goBack();
    };

    startGetReadyTimer = async () => {

        this.stopGetReadyTimer();

        if (this.state.soundEffects) {
            await countdownSound.setPositionAsync(0);
            await countdownSound.playAsync();
        }

        const getReadyTimeoutReference = setInterval(async () => {

            let remaining = this.state.timeRemaining;
            console.log(`ready time remaining: ${remaining}`);

            if (remaining <= 1) {
                this.stopGetReadyTimer();
                this.startQuestionTimer();

                console.log('startGetReadyTimer - getReadyTimeout - selectNextQuestion()');
                this.selectNextQuestion();

            } else {
                remaining = remaining - 1;
                this.setState({ timeRemaining: remaining, });
            }

        }, 1000);

        this.setState({
            playState: AppConstants.PLAY_STATE_READY,
            timeRemaining: AppConstants.TIME_GET_READY,
            getReadyTimeoutReference: getReadyTimeoutReference,
        });

    };

    stopGetReadyTimer = () => {
        clearInterval(this.state.getReadyTimeoutReference);
    };

    startQuestionTimer = () => {

        this.stopQuestionTimer();

        const questionTimeoutReference = setInterval(async () => {

            let remaining = this.state.timeRemaining;
            console.log(`answer time remaining: ${remaining}`);

            if (remaining <= 1) {

                this.stopQuestionTimer();

                // show full screen ads
                this.showFullScreenAd();

                // SHOW SCORES AFTER AppConstants.TIME_ANIMATION
                const animationTimeoutReference = setTimeout(() => {
                    this.setState({
                        playState: AppConstants.PLAY_STATE_SCORES,
                        timeRemaining: AppConstants.TIME_GET_READY,
                    });
                }, AppConstants.TIME_ANIMATION);

                console.log(`startQuestionTimer -  answers.length - ${this.state.answers.length}`);
                const answers = this.state.answers.length === 0 ? [this.createAnswer(false)] : this.state.answers;

                // SHOW TIME'S UP ERROR
                this.setState({
                    playState: AppConstants.PLAY_STATE_TIME_UP,
                    animationTimeoutReference: animationTimeoutReference,
                    answers: answers,
                });

            } else {
                remaining = remaining - 1;
                this.setState({ timeRemaining: remaining, });

                if (remaining == 5 && this.state.soundEffects) {
                    await timesUpSound.setPositionAsync(0);
                    await timesUpSound.playAsync();
                }
            }

        }, 1000);

        this.setState({
            playState: AppConstants.PLAY_STATE_PLAYING,
            timeRemaining: this.state.roundTime,
            questionTimeoutReference: questionTimeoutReference,
        });

    };

    stopQuestionTimer = () => {
        clearInterval(this.state.questionTimeoutReference);
    };

    startTiltErrorVisibleTimer = () => {
        this.stopTiltErrorVisibleTimer();

        const tiltErrorVisibleTimer = setTimeout(() => {
            this.setState({ isTiltErrorVisible: this.state.isStraight !== true });
        }, 2000);

        this.setState({
            tiltErrorVisibleTimer: tiltErrorVisibleTimer,
        });
    };

    stopTiltErrorVisibleTimer = () => {
        clearTimeout(this.state.tiltErrorVisibleTimer);
    };

    createAnswer = (answered) => {
        console.log(`createAnswer: ${answered}`);

        const question = this.state.questions[this.state.selectedIndex];
        const answer = {
            answered: answered,
            question: question,
        };
        // console.log(`created answer: ${JSON.stringify(answer)}`);

        DataRepository.markAsRead(question);

        return answer;
    };

    getAnswerColor = (item) => {
        return (item.answered === true) ? AppConstants.COLOR_WHITE : AppConstants.COLOR_RED;
    };

    playAgain = () => {
        console.log('playAgain');

        this.setState({
            playState: AppConstants.PLAY_STATE_INITIAL,
            answers: [],
        });
    };

    getBackgroundColor = (playState): string => {
        let color;

        switch (playState) {

            case AppConstants.PLAY_STATE_SUCCESS:
                color = AppConstants.COLOR_GREEN;
                break;

            case AppConstants.PLAY_STATE_PASS:
            case AppConstants.PLAY_STATE_TIME_UP:
                color = AppConstants.COLOR_RED;
                break;

            default:
                color = AppConstants.COLOR_SKY_BLUE;
                break;
        }
        return color;
    };

    isClockVisible = (playState): boolean => {
        let display = false;

        switch (playState) {

            case AppConstants.PLAY_STATE_READY:
            case AppConstants.PLAY_STATE_PLAYING:
            case AppConstants.PLAY_STATE_SUCCESS:
            case AppConstants.PLAY_STATE_PASS:
            case AppConstants.PLAY_STATE_TIME_UP:
                display = true;
                break;
        }
        return display;
    };

    isFooterVisible = (playState): boolean => {
        let display = false;

        switch (playState) {

            case AppConstants.PLAY_STATE_INITIAL:
            case AppConstants.PLAY_STATE_TIME_UP:
                display = false;
                break;

            default:
                display = this.state.isTiltErrorVisible;
        }
        return display;
    };

    displayContentIcon = (playState) => {
        let display = 'none';

        switch (playState) {

            case AppConstants.PLAY_STATE_SUCCESS:
            case AppConstants.PLAY_STATE_PASS:
            case AppConstants.PLAY_STATE_TIME_UP:
                display = 'flex';
        }
        return display;
    };

    getContentIcon = (playState): number => {
        let icon = 0;

        switch (playState) {

            case AppConstants.PLAY_STATE_SUCCESS:
                icon = require('../assets/icons/success_icon.png');
                break;

            case AppConstants.PLAY_STATE_PASS:
                icon = require('../assets/icons/error_icon.png');
                break;

            case AppConstants.PLAY_STATE_TIME_UP:
                icon = require('../assets/icons/stopwatch.png');
                break;

        }
        return icon;
    };

    getContent = (playState): string => {
        let content = "";

        switch (playState) {

            case AppConstants.PLAY_STATE_INITIAL:
                content = "Place Your Phone On Forehead";
                break;

            case AppConstants.PLAY_STATE_READY:
                content = "Get Ready";
                break;

            case AppConstants.PLAY_STATE_PLAYING:
                content = this.state.question;
                break;

            case AppConstants.PLAY_STATE_SUCCESS:
                content = "Straight Up";
                break;

            case AppConstants.PLAY_STATE_PASS:
                content = "Nah";
                break;

            case AppConstants.PLAY_STATE_TIME_UP:
                content = "Time's Up";
                break;

            default:
                content = "";
                break;
        }
        return content;
    };

    showFullScreenAd = async () => {
        if (Platform.OS === 'android') {
            AdMobInterstitial.setAdUnitID(AppConstants.ADMOB_INTERSTITIAL_IDAndroid);
        }
        else {
            AdMobInterstitial.setAdUnitID(AppConstants.ADMOB_INTERSTITIAL_IDiOS);
        }
        await AdMobInterstitial.requestAdAsync();
        await AdMobInterstitial.showAdAsync();
    };

    isRunningOniOSDevice = (): string => {

        if (Platform.OS === 'android') {
            return AppConstants.ADMOB_BANNER_IDAndroid;
        }
        else {
            return AppConstants.ADMOB_BANNER_IDiOS;
        }
    }
}
