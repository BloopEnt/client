import React from 'react';
import { Dimensions, StatusBar, Image, View, Platform, } from 'react-native';

import * as Expo from "expo";
import AppConstants from '../utils/AppConstants';
import DataRepository from "../data/DataRepository";

const window = Dimensions.get('window');
const height = window.height;
const width = window.width;
const isLandscape = height > width;

export default class HomeScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            fullWidth: (isLandscape) ? height : width,
            fullHeight: (isLandscape) ? width : height,
        };
    }

    render() {
        return (
            <View style={{ backgroundColor: AppConstants.COLOR_BLACK, flex: 1, }}>
                <Image
                    source={require('../assets/splash.png')}
                    style={{
                        width: this.state.fullWidth,
                        height: this.state.fullHeight,
                        resizeMode: 'contain',
                    }} />
            </View>
        );
    }

    async componentWillMount() {

        const orientationForPlatform = Platform.OS === 'android' ? Expo.ScreenOrientation.Orientation.LANDSCAPE_LEFT : Expo.ScreenOrientation.Orientation.LANDSCAPE_RIGHT;
        Expo.ScreenOrientation.allow(orientationForPlatform);

        setTimeout(() => {

            // show home screen
            this.props.navigation.replace(AppConstants.Screens.HOME);

            // TODO: comment after testing
            // this.props.navigation.navigate(AppConstants.Screens.PLAY_GAME, {
            //     subcategory: { "uid": "14", "iosInAppId": "", "name": "80s", "andrdoidInAppId": "", "category": "18", "price": "0", "paid": false, "background": "#467f94", "status": "1" },
            //     customCategory: false,
            // });

        }, 2000);

        StatusBar.setHidden(true);
        Dimensions.addEventListener('change', this.dimensHandler);

        // // Load fonts
        // await Expo.Font.loadAsync({
        //     Montserrat: require("../assets/fonts/MontserratRegular.ttf"),
        // });

        // Play startup sound
        const enabled = await DataRepository.getSoundEnabled();
        if (enabled) {
            const startupSound = new Expo.Audio.Sound();
            await startupSound.loadAsync(AppConstants.SOUND_SPLASH);
            await startupSound.playAsync();
        }

        // // TODO: delete after testing

        // // SAVE PURCHASES
        // const items = ["item1", "item2"];
        // DataRepository.savePurchases(items)
        //     .then(saved => {
        //         // console.log(`purchases saved: ${items}`);
        //     })
        //     .catch(error => {
        //         // console.log(`error in saving purchases: ${error}`);
        //     });

        // const iapId = Platform.OS === 'ios' ? "item1" : "item1";
        // DataRepository.isPurchased(iapId)
        //     .then(purchased => {
        //         console.log(`isPurchased: ${iapId} = ${purchased}`);
        //     })
        //     .catch(error => {
        //         console.log(`error in getting saved purchase: ${iapId} = ${error}`);
        //     });
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.dimensHandler);
    }

    /**
     * Handle dimensions changes
     * */
    dimensHandler = dimens => {
        let height = dimens.window.height;
        let width = dimens.window.width;
        let isLandscape = height > width;

        this.setState({
            fullWidth: (isLandscape) ? height : width,
            fullHeight: (isLandscape) ? width : height,
        });
    };


}