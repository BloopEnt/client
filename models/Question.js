class Question {

    uid: string;
    name: string;
    subcategory: string;
    createTime: string;
    updateTime: string;
    status: string;

    constructor(uid: string, name: string, subcategory: string, createTime: string, updateTime: string, status: string) {
        this.uid = uid;
        this.name = name;
        this.subcategory = subcategory;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.status = status;
    }

}

export default Question;