export default ResourceStatus = {
    Loading: 0,
    Success: 1,
    Error: 2,
};