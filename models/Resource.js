import React from "react";
import ResourceStatus from "./ResourceStatus";

class Resource {

    constructor(status, data, message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    static loading(data) {
        return new Resource(ResourceStatus.Loading, data, "");
    }

    static success(data) {
        return new Resource(ResourceStatus.Success, data, "");
    }

    static error(data, message) {
        return new Resource(ResourceStatus.Error, data, message);
    }
}

export default Resource;