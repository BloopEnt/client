class Category {

    uid: string;
    name: string;
    icon: string;
    background: string;
    status: string;
    instructions: string;
    index: number;

    iosInAppId: string;
    andrdoidInAppId: string;
    price: number;
    paid: boolean;

    createTime: string;
    updateTime: string;

    constructor(uid: string, name: string, createTime: string, updateTime: string, icon: string, background: string, status: string, instructions: string, index: number, iosInAppId: string, andrdoidInAppId: string, price: number, paid: boolean) {
        this.uid = uid;
        this.name = name;
        this.background = background;
        this.icon = icon;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.status = status;
        this.instructions = instructions;
        this.index = index;

        this.andrdoidInAppId = andrdoidInAppId;
        this.iosInAppId = iosInAppId;
        this.price = price;
        this.paid = paid;
    }

}

export default Category;