
export default class SubCategory {

    uid: string;
    name: string;
    category: string;
    background: string;
    status: string;
    price: number;
    paid: boolean;
    andrdoidInAppId: string;
    iosInAppId: string;
    createTime: string;
    updateTime: string;

     constructor(uid: string, iosInAppId: string, name: string, andrdoidInAppId: string, category: string, price: number, paid: boolean, createTime: string, updateTime: string, background: string, status: string) {
        this.iosInAppId = iosInAppId;
        this.andrdoidInAppId = andrdoidInAppId;
        this.price = price;
        this.paid = paid;

        this.uid = uid;
        this.name = name;
        this.category = category;
        this.background = background;
        this.status = status;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

}